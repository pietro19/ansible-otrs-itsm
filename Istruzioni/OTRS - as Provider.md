#OTRS as Provider#
##Remote Request:##

- HTTP request
- OTRS receives HTTP request and passes it through the layers.
- The provider module is in charge to execute and control these actions.

##Network Transport##

- The network transport module decodes the data payload and separates the operation name from the rest of the data.
- The operation name and the operation data are returned to the provider.
- Data External
- Data as sent from the remote system (This is not a module-based layer).

##Mapping##

- The data is transformed from the External System format to the OTRS internal format as specified in the mapping configuration for this operation (Mapping for - incoming request data).
- The already transformed data is returned to the provider.
- Data Internal
- Data as transformed and prepared to be passed to the operation (This is not a module based layer).
##Operation##

- Receives and validates data.
- Performs user access control.
- Executes the action.

#OTRS Response:#
##Operation##

- Returns result data to the provider.
- Data Internal
- Data as returned from operation.

##Mapping##

- The data is transformed back to the Remote system format as specified in the mapping configuration (Mapping for outgoing response data).
- The already transformed data is returned to the provider.

##Data external##

- Data as transformed and prepared to be passed to Network Transport as response.

##Network Transport##

- Receives the data already in the Remote System format.
- Constructs a valid response for this network transport type.

##HTTP response##

- The response is sent back to the web service client.
- In the case of an error, an error response is sent to the remote system (e.g. SOAP fault, HTTP error, etc).