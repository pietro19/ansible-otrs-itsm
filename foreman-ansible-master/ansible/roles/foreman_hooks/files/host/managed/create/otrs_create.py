#!/usr/bin/env python
import sys
import tempfile
import subprocess

sys.path.append('/usr/share/foreman/config/hooks')

import json
import requests
from requests.auth import HTTPBasicAuth
from subprocess import call

from functions import (HOOK_EVENT, HOOK_OBJECT, HOOK_TEMP_DIR, get_json_hook)

otrs_base_url = "http://x-otrs-cmdb.iks.local/otrs/nph-genericinterface.pl/Webservice/TestTest/"
otrs_search_ci_method = "ConfigItemSearch"
url_search = "%s%s" % (otrs_base_url, otrs_search_ci_method)
get_ci_method = "ConfigItemGet"
update_ci_method = "ConfigItemUpdate"
create_ci_method = "ConfigItemCreate"

url_get = "%s%s" % (otrs_base_url, get_ci_method)
url_update = "%s%s" % (otrs_base_url, update_ci_method)
url_create = "%s%s" % (otrs_base_url, create_ci_method)

user = "root@localhost"
pwd = "asdfgh"
user_id = 1

HOOK_JSON = get_json_hook()

# read the information received
if HOOK_JSON.get('host'):
    hostname = str(HOOK_JSON.get('host').get('host').get('name', None))

    mac_address = str(HOOK_JSON.get('host').get('mac', None))

    operating_system = str(HOOK_JSON.get('host').get('host').get('operatingsystem_name', None))

    ip = str(HOOK_JSON.get('host').get('host').get('ip', None))

    payload_create = {
        "UserLogin": user,
        "Password": pwd,
        "ConfigItem": {
            "Class": "Virtual Machine",
            "Name": hostname,
            "DeplState": "Production",
            "InciState": "Operational",
            "CIXMLData": {
                "CPU": 
                    {
                        "CPU": "",
                        "Cores": "",
                        "Number": ""
                    },
                "Datastore": "",
                "ExposedServices": "--None--",
                "FQDN": hostname,
                "GuestOS": operating_system,
                "HardwareMachineVersion": "",
                "IPoverDHCP": "Yes",
                "IPv4Address": ip,
                "IPv6Address": "None",
                "InstalledSoftware": "",
                "Owner": "",
                "Ram": "",
            }
        }
    }

    response_get = requests.post(url_create, json=payload_create)
