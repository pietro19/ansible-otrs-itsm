# Foreman without Puppet: #

1) Can we use Foreman without Puppet ? Just as Inventory Mgmt Tool 

	Yes, it can be used without puppet, but not really for inventory, as 
	the inventory facilities largely rely on cfg management tools to feed 
	in the inventory data. Standalone Foreman is useful for provisioning 
	baremetal, kvm/xen, vmware, and various cloud instances. 

		All absolutely true, however, the fact (inventory) API endpoint is now 
		pure json, so it's quite possible to upload data from your own 
		inventory system, so long as you make it fit the format Foreman is 
		looking for. We can go into more detail if need be, but a simple fact 
		hash in json form is a how Puppet represents it's facts. 

The installer does require Puppet and it will set up a Puppet master by default, but Foreman itself doesn't require Puppet. It's entirely possible to install Foreman either from packages or source without Puppet.

If you're unfamiliar with how Foreman works, or how you'd configure it from scratch, I'd strongly recommend sticking with the installer despite this, as you probably won't set it up correctly or optimally otherwise (e.g. it configures Passenger, SSL etc.). Get to know it in the "default" configuration before going further.

You could run the installer to set it up, then remove the Puppet master virtualhost, delete the installer and Puppet packages, and disable the Puppet module in the smart proxy (/etc/foreman-proxy/settings.d/puppet.yml). The installer would still use the Puppet certificates it generates for authentication and encryption between the main Foreman server and the smart proxy service.

You could also run the installer on a host to gain some familiarity with the application and then use the experience and existing host as a reference to set it up again without Puppet.

You can register already existing hosts in Foreman and check out a history of your Ansible reports via Foreman Ansible (theforeman.org/plugins/foreman_ansible/0.x/index.html) too. It'd be the equivalent of 'puppet-agents calling home' as you said before – eLobato Apr 21 '16 at 9:24

Sources:
https://stackoverflow.com/questions/40305150/issue-with-forman-provisioning-without-puppet-agent
https://groups.google.com/forum/#!topic/foreman-users/7mlJwZrLrnI
https://serverfault.com/questions/771578/foreman-without-puppet
https://www.reddit.com/r/ansible/comments/574pn9/ansible_and_foreman_anyone_using/?st=jbxnxj7k&sh=11b83993