E' stato configurato partendo da:

- <http://docs.ansible.com/ansible/latest/intro_dynamic_inventory.html>
- <https://github.com/theforeman/foreman_ansible_inventory>

Per poterlo utilizzare sono state controllati gli import nel python file, di conseguenza è stato installato **configparse** con il comando che segue:

	yum install python2-configargparse