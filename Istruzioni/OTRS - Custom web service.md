# Nuovo Web Service per OTRS#
<https://translate.google.it/translate?hl=it&sl=de&tl=en&u=http%3A%2F%2Fforums.otterhub.org%2Fviewtopic.php%3Ff%3D35%26t%3D32710%26p%3D133041&sandbox=1>
<https://stackoverflow.com/questions/34675372/how-to-link-get-config-item-to-a-ticket-through-webservice-soap-or-rest-in-o>

## Creare il file XML per la registrazione del modulo ##
Copiarlo dalla pagina di StackOverflow, nella cartella segnata nella guida

Per la versione 6:
- Convertirlo secondo la sintassi descritta <http://doc.otrs.com/doc/manual/developer/stable/en/html/how-it-works.html#xml-config>, copiarlo in Files/XML
- Ricostruire la configurazione di OTRS, come spiegato qui: <http://doc.otrs.com/doc/manual/admin/6.0/en/html/configuration.html>, con il comando:
	
	perl bin/otrs.Console.pl Maint::Config::Rebuild

## Abilitare il modulo appena creato ##
Andare su Admin > System Configuration > GenericInterface > Operation > ModuleRegistration, abilitare il modulo che è stato registrato allo step precedente-
Eseguire il deploy delle modifiche.

## Creare il Web Service ##
All'interno di Admin > Web Service Management, si può andare a creare il nuovo servizio che verrà usato per esegurire le operazioni dall'esterno.
Per i dettagli dei web services, si può far riferimento a <http://doc.otrs.com/doc/manual/admin/6.0/en/html/genericinterface.html>.

Nella parte **OTRS as Provider**, si trova il menù *Add Operation*, qui si può selezionare il tipo d'operazione che il Web Service permetterà.
Nel caso attuale, si desidera l'apertura di un ticket associato ad un CI.
Il tipo di trasporto selezionato è *SOAP*.
Nelle operazoni il mapping dei dati in ingresso ed in uscita è *Simple*, la regola per le chiavi ed i valori non mappati prevede di tenerli (Keep - leave unchanged).

