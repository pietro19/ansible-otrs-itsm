# -*- coding: utf-8 -*-
from __future__ import print_function
try:
    # Python 2 version
    import ConfigParser
except ImportError:
    # Python 3 version
    import configparser as ConfigParser

import json, argparse, copy, os, re, sys, requests, datetime, time, csv, operator, difflib

from time import time
from collections import defaultdict
from distutils.version import LooseVersion, StrictVersion
from requests.auth import HTTPBasicAuth
from subprocess import call

if LooseVersion(requests.__version__) < LooseVersion('1.1.0'):
    print('This script requires python-requests 1.1 as a minimum version')
    sys.exit(1)


def json_format_dict(data, pretty=False):
    """Converts a dict to a JSON object and dumps it as a formatted string"""
    
    if pretty:
        return json.dumps(data, sort_keys=True, indent=2)
    else:
        return json.dumps(data)

class ForemanSnipe(object):
#Creazione array di facts  degli host di Foremna
    def __init__(self):
        self.inventory = defaultdict(list)  # A list of groups and the hosts in that group
        self.cache = dict()   # Details about hosts in the inventory
        self.params = dict()  # Params of each host
        self.facts = dict()   # Facts of each host
        self.hostgroups = dict()  # host groups
        self.hostcollections = dict()  # host collections
        self.session = None   # Requests session
        self.config_paths = ["./ForemanSnipe.ini"]
        self.snipe_hosts = dict()
        self.foreman_hosts = []
        self.removed_hosts = []
        
    def read_settings(self):
        """Reads the settings from the foreman.ini file"""

        config = ConfigParser.SafeConfigParser()
        config.read(self.config_paths)

        #Snipe API related
        try:
            self.snipe_url = config.get('snipe', 'url')
            self.snipe_api_token = config.get('snipe', 'api_token')
            self.snipe_headers = {"Authorization":"Bearer " + self.snipe_api_token, "Accept":"application/json"}
        except (ConfigParser.NoOptionError, ConfigParser.NoSectionError) as e:
            print("Error parsing configuration: %s" % e, file=sys.stderr)
            return False

        # Foreman API related
        try:
            self.foreman_url = config.get('foreman', 'url')
            self.foreman_user = config.get('foreman', 'user')
            self.foreman_pw = config.get('foreman', 'password')
            self.foreman_ssl_verify = config.getboolean('foreman', 'ssl_verify')
        except (ConfigParser.NoOptionError, ConfigParser.NoSectionError) as e:
            print("Error parsing configuration: %s" % e, file=sys.stderr)
            return False

        return True

    def parse_cli_args(self):
        """Command line argument processing"""

        parser = argparse.ArgumentParser(description='Produce an Ansible Inventory file based on foreman')
        parser.add_argument('--list', action='store_true', default=True, help='List instances (default: True)')
        parser.add_argument('--host', action='store', help='Get all the variables about a specific instance')
        # parser.add_argument('--refresh-cache', action='store_true', default=False,
        # help='Force refresh of cache by making API requests to foreman (default: False - use cache files)')
        self.args = parser.parse_args()

    def _get_session(self):
        if not self.session:
            self.session = requests.session()
            self.session.auth = HTTPBasicAuth(self.foreman_user, self.foreman_pw)
            self.session.verify = self.foreman_ssl_verify
        return self.session

    def _get_json(self, url, ignore_errors=None, params=None):
        if params is None:
            params = {}
        params['per_page'] = 250

        page = 1
        results = []
        s = self._get_session()
        while True:
            params['page'] = page
            ret = s.get(url, params=params)
            if ignore_errors and ret.status_code in ignore_errors:
                break
            ret.raise_for_status()
            json = ret.json()
            # /hosts/:id has not results key
            if 'results' not in json:
                return json
            # Facts are returned as dict in results not list
            if isinstance(json['results'], dict):
                return json['results']
            # List of all hosts is returned paginaged
            results = results + json['results']
            if len(results) >= json['subtotal']:
                break
            page += 1
            if len(json['results']) == 0:
                print("Did not make any progress during loop. "
                      "expected %d got %d" % (json['total'], len(results)),
                      file=sys.stderr)
                break
        return results

    def _get_hosts(self):
        url = "%s/api/v2/hosts" % self.foreman_url

        params = {}

        return self._get_json(url, params=params)

    def get_inventory(self):
        
        self.groups = dict()
        self.hosts = dict()
        
        for host in self._get_hosts():
            dns_name = host['name']
            self.foreman_hosts.append(str(dns_name))

    def get_snipe_data(self):
        hdrs = self.snipe_headers
        url=self.snipe_url
        response = requests.get(self.snipe_url, headers=hdrs)
        response_native = json.loads(response.text)["rows"]
        for host in response_native:
            if(host["model"]['id']==86):
                # self.snipe_hosts.update({host["asset_tag"]: host["id"]})
                self.snipe_hosts[str(host["asset_tag"])] = host["id"]

    def remove_snipe_host(self, ids_to_remove):
        hdrs = self.snipe_headers
        url = self.snipe_url
        result = ""
        for id_value in ids_to_remove:
            url_asset = "%s/%s" % (url, id_value)
            response = requests.delete(url_asset, headers=hdrs)
            response_native = json.loads(response.text)
            result += "Asset: %s message: %s\n" % (id_value, response_native['messages'])

        return result



    def find_removed_hosts(self):
        # trova gli host per cui l'fqdn non è presente nell'altra lista e restituisci lo snipe-id
        #liste utilizzate: foreman_hosts, snipe_hosts
        current_snipe_hosts = []

        for key in self.snipe_hosts:
            # print("key: %s , value: %s" % (key, self.snipe_hosts[key]))
            current_snipe_hosts.append(key)

        print("current_snipe_hosts")
        print(current_snipe_hosts)
        print("self.foreman_hosts")
        print(self.foreman_hosts)
        
        # diff2 = difflib.unified_diff(current_snipe_hosts, self.foreman_hosts, lineterm='', n=0)
        # lines2 = list(diff2)[2:]
        # print(lines2)
        # removed = [line[1:] for line in lines2 if line[0] == '-']
        # print("removed")
        # print(removed)
        removed = [item for item in current_snipe_hosts if item not in self.foreman_hosts]
        print(removed)
        for host_fqdn in removed:
            self.removed_hosts.append(self.snipe_hosts[host_fqdn])


    def run(self):
        # Read settings and parse CLI arguments
        if not self.read_settings():
            return False
        self.parse_cli_args()
        self.get_inventory()
        self.get_snipe_data()
        self.find_removed_hosts()
        print("self.removed_hosts")
        print(self.removed_hosts)
        to_print = self.remove_snipe_host(self.removed_hosts)
        print(to_print)
        return True

ForemanSnipe().run()