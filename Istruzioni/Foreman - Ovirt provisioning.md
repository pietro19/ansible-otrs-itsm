#Foreman - Ovirt provisioning#

Per creare una nuova VM è necessario configurare alcuni parametri: alcuni si trovano in *Hosts -> Provisiong setup*, altri sono all'interno del tab *Infrastructure*

1. Configurare il Sistema Operativo: quelle che seguono sono le indicazioni per CentOS 7.4
	- Aggiungere Nome (CentOS), Versione maggiore (7) e minore (4) famiglia (RedHat), 
	- Aggiungere un template per le Partition Table, (Kickstart default, che è valido per la famiglia RedHat)
	- Aggiungere l'installation media (CentOS mirror)
	- Aggiungere i template di provisioning (Kickstart default), PXELinux template (Kickstart default PXELinux), ed un finish template (ne è stato creato uno per aggiungere automaticamente le chiavi SSH i dettagli sono a fine pagina).

2. Controllare Hosts -> Architectures
	Che sia presente x86_64, associa a ai Sistemi Operativi CentOS

3. All'interno di Hardware Models dovrebbero essere presenti dei modelli per le compute resources configurate, in particolare un oVirt Node, e VMware Virtual Platform.

4. Passando poi alla parte di Infrastructure, si possono controllare i Domains presenti: grazie agli host aggiunti precedentemente si ha:
	- iks.local
	- ovirt.iks.lab

5. Si possono quindi configuare le Subnets, ne sono state create due, partendo dai parametri di configuarazione utilizzati nella creazione di una VM con oVirt, ed utilizzando un intervallo d'indirizzi riservato ai test, nella maniera che segue:
	1. ovirt.iks.lab: 
		- Protocollo: IPV4
		- Network Address: 192.168.141.0
		- Network Prefix: 24
		- Network Mask: 255.255.255.0
		- Gatway Address: 162.168.141.1
		- Primary DNS Server: 192.168.141.2
		- IPAM: DHCP
			- From: 192.168.141.181
			- To: 192.168.141.210
		- Boot Mode: Static
		- Domains: ovirt.iks.lab
		- Proxies > DHCP Proxy: capomastro.ovirt.iks.lab
		- Proxies > TFTP Proxy: capomastro.ovirt.iks.lab
	2. iks.local: 
		- Protocollo: IPV4
		- Network Address: 192.168.141.0
		- Network Prefix: 24
		- Network Mask: 255.255.255.0
		- Gatway Address: 162.168.141.1
		- Primary DNS Server: 192.168.141.2
		- IPAM: DHCP
		- Boot Mode: Static
		- Domains: iks.local

6. Compute Resources: servono a creare e gestire host su servizi cloud e virtualizzazione. In capomastro.ovirt.iks.lab, sono stati configurati oVirt e VMware: per i dettagli basta far riferimento al documento **Foreman - whitout Puppet - installation.md**: si può controllare che l'installazione sia andata a buon fine controllando *Administer > About > Available Providers*, in cui sono segnalati i providers installati. 
Premendo il pulsante *Create Compute Resource*, si può procedere a caricare i paramentri necessari:
	- Per ovirt:
		- Name: ovirt.iks.lab
		- Provider: oVirt
		- Url: ovirt.iks.lab
		- User: the_user_fof_foreman_in_otrs
		- Password: its_password
		Premendo poi il pulsante *Load Datacenters* vengoni testati i paramentri per la connessione, e viene popolato il campo relativo a X509 CA.
	- Per VMware: è stato creato un utente con dei permessi di sola lettura sugli host di VMware, fare riferimento a: <https://theforeman.org/manuals/1.16/index.html#5.2.9VMwareNotes>

# Creazione di un nuovo host: #

Dopo aver configurato i parametri di cui sopra, diventa possibile creare un host; l'esempio che segue riguarda oVirt.
Dal pannello Hosts > Create Host, viene presentata una form da cui iniziare:

1. Host:
	- name: x-pietro
	- Host Group: 
	- Deploy on: ovirt.iks.local(oVirt)
2. Virtual Machine:
	- Cluster: Default
	- Tempalte: TEMPLATE_CENTOS7_CLOUD_INIT
	- Cores: [selezionare a seconda delle necessità]
	- Memory: [selezionare a seconda delle necessità]
	- Network interfaces > Storage > [nulla, il template ha già un HD da 10GB che può fare boot]
3. Operating System:
	- Architecture: x86_64
	- Operating system: CentOS 7.4
	- Provisiong Method: Network Based
	- Build: (unchecked)
	- Media: CentOS Mirror
	- Partition Table: Kickstart default
	- PXE loader: PXELinux BIOS
	- Root pass: [inserire una password con più di 8 caratteri, il template però la imposterà a '*qwerty*']
	
	Premere infine Provisiong Templates > Resolve: controlla la presenza dei template necessari, che per un SO parte di Red Hat sono un PSELinux, un Provisioning ed un Finish Template.
4. Interfaces > Interface > edit
	-Device Identifier: eth0
	- DNS Name: [lasciare il nome di default, lo stesso di *Host>Name*]
	- Domain: ovirt.iks.lab
	- IPv4 Subnet: ovirt.iks.lab (192.168.141.0/24)
	- IPv4: [si configura con la subnet]
	- Managed: Yes
	- Primary: Yes
	- Provision: Yes

# Import di un host#

Il processo è abbastanza lineare: è sufficiente recarsi nel provider desiderato, in *Infrastructure > Compute Resources*, controllare la scheda *Virtual Machines*, ed una volta trovata quella desiderata, le strade sono:

- visualizzarene i dettagli premendo sul nome e da qui, il pulsante **Associate VM**
- aprire il menu relativo all VM, con la freccia verso il basso e selezionare **Import**
In entrambi i casi, è quindi possibile completare l'import configurando i parametri dell'host, aggiungerlo ad un host group ed assegnare degli asnible-roles.

## Template per configurare le chiavi SSH sugli host ##

- Nome: Add ssh keys
- Sintassi: sh
- Tipo: Finish Template
- Associazione: CentOS 7.3, 7.4
- Contenuto:

```	
#!/bin/bash
mkdir -p .ssh
cat << EOF >> .ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAgEA42K8fg6K1VaKsSqvBXPeg1os2X8s6jy4GSjHzX2uPpVIsB1X0ClYAx3MkReJJ7hIHuWZO0/Zm1s5JcjbGksIr2B/j/oeUGj7cL70x7U2/LfQ/jmxHEzTPfCtqww2vz/Yxcvx8qhAiDF3fw/tWLQQSucd/F3vDNNhZZu7l4CZb5tf0Ua8n27DGjeMxLvQTF5ACgF8CLQWs8R1d530WYpdSZWvj94ya4DevqZC6lqaQNgRFghvEm3wmybhfVWjanKf5KRNj06ySYIoUBizPDNta4Gzgf7BRNOhdusxg4sqo/Y9EW79R0ejBJxH2/emIDFgRALSzxGMZXTvIjE/aOdxlidndJm7r6gGDSYEbV8aKfLQhFxIqPieAECXajOjuGhT/cpeupBnfGXuhetAgRKXHBgRbpL3ROUrD+xA+cDXUjpUZ5RuyLJ4/DVxJjhv9LyvhBQu+qos+WhudU4FQR3uX/B/ioNpkr/xtMEPFr2JWeAF5heFK8rs61zItsCi+x3WHY0c1w+h2TFQjB6n1et/kIIeiOIXmQ/G5H0PufogU9Vl1St3bzaKK2Bg64YubMSbzFWuA9oTJ1IPGSiMJ/HP81Jez5IpXVopv0xQq8qgcWDNfX8jIQ9QwEBi/ZRjI/9tINBB2oBtdqRTt/13EvTtIoagcN9XO4Sz1mbbROONFU8= rsa-key-20150823-x-00398
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDWJgE/MNRgaVgiBgJf59MP1ejhWKO4TI/F+/ODzkTMjTm1t6hAvxMsDKti3XoHFDWlaxyVPYux33CDBWINa/en+E5Fkws+3QuntET8dfKNBHF0xmHzSsK2NlIRF2Dy/GyO3cLSlGHp6B3DaaQl9Qu5sYlPrlwRgMXV8V6lRmQuuf0h+ZlgvZzj2Kj6uiC0wbaIsMdMoI+Hiudc1QBDIm4/BVy7MQU/dPMgWESVG+SmZcjE/WLbs3AtKjSGVoB1Nf9Ir3ltL56g5ksn9vff/gFsWOyrFJCRG61f5PoNieop9IrbDlPrLqD2JQov8bCmX6fKQH5vt6C2EYfnVPasT3Fx root@services.ovirt.iks.lab
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCmnPRlSqeqYUxh9NShkCPWt6mpW14kxQP46HKJUF2CWe6gpoikv+b868nAUVlFhJxu0ri7Fo2AWEiODlthhBVftjgj8vwWYBa7i85r3EJU8Lac39lEGE8l3X24BpOfHN9vLk//oqpOEqXhrRyZH6Fw65RuMOutyjIHxvVcDcRrs/PddlgXLKIVqJryGsA2GTBqYQaV8Xjx+Frjb1T9Qe8WEmeTLjgeurE0+52CYt7G/zH+oo3OKfxcOdMIQelC12gaRQDJDHmAj+vgE3k2aMVSCHTKjbmZlzgBic0BptDAENuTm3i6OZLx5251u/4ZOmGfoeUoXup9T000nU9h7+JlGKohpEAyBk5m8Z33bc0dWU2Qlg8cKCGwHsfNJWn4PiHtlcBnrYKH1GpXiimYaRf+j1readTLeHwE+FjKsuhVJpaJsHjG0s0pX3qDJkNTP60x3y0+ywBciNYuY3VMZ3824PlNsJijBj6g1psZ+PzD/UeCOgLSPwbA7ObmWexdsmnAyB5K/qdhaw5RweHPxaYRZWg9Fx3Cmw3T4XZXOR2tj3EdKLxDH/TVwnydsQyzVLm9KG70Bm3JvILovDIlu8HDCePuImvtb/H5kvORpa6o1jxDgx77SHSXxYNGNp74jokF39vwy6XxaYO7Ziqe8fpWMW6zIw8Qk4tXZvEbCZiwqQ== root@capomastro.ovirt.iks.lab
EOF
chmod og-rwx -R .ssh
```