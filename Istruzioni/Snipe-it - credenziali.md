# Snipe-it: #

server: **[snipeit.ovirt.iks.lab]**

user: root
pw: qwerty

*Snipe-it:*
Creazione nuovo utente admin
php artisan snipeit:create-admin --first_name=Pietro --last_name=Gabelli  --email=pietro.gabelli@iks.it  --username=pgabelli  --password=qwerty

*Credenziali d'accesso*
	user: pgabelli
	pw: qwerty

*Token*
	name: Pietro

	token:
		eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjM0YjY4ZmI0MDQyYzczMGE1ZDA5Y2E4OTcyMWMxNzEzMzMyN2E3YjBjYmRhZjBhY2Q0NGEwMTJjYTc4NWRkMzE4ZTU1MTUxMGFkNzg3M2QyIn0.eyJhdWQiOiIxIiwianRpIjoiMzRiNjhmYjQwNDJjNzMwYTVkMDljYTg5NzIxYzE3MTMzMzI3YTdiMGNiZGFmMGFjZDQ0YTAxMmNhNzg1ZGQzMThlNTUxNTEwYWQ3ODczZDIiLCJpYXQiOjE1MTM3MDA2NTksIm5iZiI6MTUxMzcwMDY1OSwiZXhwIjoxNTQ1MjM2NjU5LCJzdWIiOiIyNDYiLCJzY29wZXMiOltdfQ.eXjH2wyGXIIZYlnlPot5JUFfuSlaIhIJh1WGxdj7aDJwW8ZIga95UGJBMKpDWiS1x5oItTLINykdKCRoNQNartcJFNcyDSNFzl0MuWuNH6v5BoAKsAhNmeD7T9lEKsBQzW1JToWr37ik6ePBgovdk24-oltxMYVHc43sMILm-jwrSRTyxMoBK4BxWBNe3UjIgbxlP7QoaWwq1enyjOemKjRW3dSC-Oq4tD6m-Q6xbsza6AWklvk_NC_mFhcUVVElAATLBjwzLHYZ9svE4C_8KsqfgbewhFoqYCn1HLRIfS3q0mz1ofjg0gBDqfCPFIj803pBLZgm-JcQyTgPHPcr269eJhKDrZHRutknxtb7uYmv8voU9u_Gyzabt2RiVMBuiClECJwXOKwDS3KJbuiDNYCD_fO1FrRlyGOn9_Y4YOsMlU8BCxslWf_MvHk9qxu_A5Q-14HalHQcYvFQFs4xDcuzS48Tdt_97lL9OBiessZeRpvrNXJxk0pqkDiGwVcdfUGApuoUGEa-NGyizvxZb2PFis-CO3ykNSCXW4c2-yfDr_uwnfqqUQsVyzoUbHGTwSckj9sM_ZG7geIdxBxDkK4Q6tgxGgQA83zsDzvzKJ2Ava7pSjUpXSmbsvrjnvjkv_FnMgYzR-MMboWLLYN1U_iNLBGPFX5utB5PREnpkyI

## Snipe-it API: ##
[https://snipe-it.readme.io/v4.1.6/reference#hardware-list]

### Snipe-it Import CSV ###
[https://snipe-it.readme.io/v4.1.6/docs/importing]

Comando:
	Dalla cartella var/www :
	
	php artisan snipeit:import path/to/your/file.csv 
Oppure

	php /var/www/snipe-it/artisan snipeit:import /var/www/snipe-it/prova_upload.csv

### Contenuto del file: ###
Name,Email,Username,item Name,Category,Model name,Manufacturer,Model Number,Serial,Asset Tag,Default Location,Location,Notes,Purchase Date,Purchase Cost,Company,Status,Warranty,Supplier
Pietro Gabelli,,,PC Prova 2,Laptop,Probook 4530s,HP,,CNU2030W55N,10305,Padova,Padova,PC prova,20/02/2012,NULL,IKS,Deployed,NULL

### Snipe-it Add item ###
1. Raccolta dati con Ansible: modifica callback script, da far girare - inizialmente - sul singolo host
2. Organizzazione dati dell'host nella variabile *request*
3. Invio dei dati con una POST

Fonti:
	[https://snipe-it.readme.io/v4.1.6/reference#hardware-create ]
	[http://docs.python-requests.org/en/master/user/quickstart/#custom-headers ]
	[https://snipe-it.readme.io/v4.1.6/reference#hardware-list ]
	[https://snipe-it.readme.io/v4.1.6/docs/importing ]