lab-fs.ovirt.iks.lab	ansible_user=otrs-cmdb
services.ovirt.iks.lab	ansible_user=otrs-cmdb


elastic-fe-01.ovirt.iks.lab  ansible_user=otrs-cmdb	ansible_pass=otrscmdb2017
elastic-fe-02.ovirt.iks.lab  ansible_user=otrs-cmdb
elastic-fe-03.ovirt.iks.lab  ansible_user=otrs-cmdb
elastic-be-01.ovirt.iks.lab  ansible_user=otrs-cmdb
elastic-be-02.ovirt.iks.lab  ansible_user=otrs-cmdb
elastic-be-03.ovirt.iks.lab  ansible_user=otrs-cmdb
elastic-ls-01.ovirt.iks.lab  ansible_user=otrs-cmdb



ssh-copy-id otrs-cmdb@lab-fs.ovirt.iks.lab
ssh-copy-id otrs-cmdb@services.ovirt.iks.lab



ssh-copy-id otrs-cmdb@elastic-fe-01.ovirt.iks.lab
ssh-copy-id otrs-cmdb@elastic-fe-02.ovirt.iks.lab
ssh-copy-id otrs-cmdb@elastic-fe-03.ovirt.iks.lab
ssh-copy-id otrs-cmdb@elastic-be-01.ovirt.iks.lab
ssh-copy-id otrs-cmdb@elastic-be-02.ovirt.iks.lab
ssh-copy-id otrs-cmdb@elastic-be-03.ovirt.iks.lab
ssh-copy-id otrs-cmdb@elastic-ls-01.ovirt.iks.lab



usr: otrs-cmdb
pwd: otrscmdb2017


ansible 'otrs-cmdb:otrscmdb2017@lab-fs.ovirt.iks.lab,' -m ping

ssh-copy-id -i ~foreman-proxy/.ssh/id_rsa_foreman_proxy.pub otrs-cmdb@lab-fs.ovirt.iks.lab