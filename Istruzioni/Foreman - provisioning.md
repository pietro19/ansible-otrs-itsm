Configurazione server TFTP
https://projects.theforeman.org/projects/smart-proxy/wiki/Tftp

CentOS PXE setup:
https://wiki.centos.org/HowTos/PXE/PXE_Setup
La cartella di syslinux è: /usr/share/syslinux

Chiusura server, con https:
https://projects.theforeman.org/projects/foreman/wiki/Setting_up_Nginx_+_Passenger_

CentOS minimal ovirt template (non utilizzato)
https://github.com/rharmonson/richtech/wiki/CentOS-7-1511-Minimal-oVirt-Template

