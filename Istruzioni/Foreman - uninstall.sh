#!/bin/bash

# Remove foreman
yum remove  tfm-rubygem-foreman_ansible foreman foreman-installer foreman-proxy
rm -rf /var/lib/foreman /usr/share/foreman /usr/share/foreman-proxy/logs /etc/foreman /etc/foreman-installer /etc/foreman-proxy
rm /etc/httpd/conf.d/foreman.conf 

# Remove puppet
yum remove puppet puppetmaster puppet-common puppetmaster-common puppetlabs-release
rm -rf /usr/lib/ruby/vendor_ruby/puppet /usr/share/puppet /var/lib/puppet /etc/puppet
rm /etc/apache2/conf.d/puppet.conf