php artisan snipeit:backup

php artisan down

mv snipe-it snipe-it-backup

git clone https://github.com/snipe/snipe-it snipe-it

chown nginx:nginx snipe-it

composer install --no-dev --prefer-source
composer dump-autoload

cp -R snipe-it-backup/public/uploads/* snipe-it/public/uploads
cp -R snipe-it-backup/storage/private_uploads/* snipe-it/storage/private_uploads
cp -R snipe-it-backup/storage/app/backups/* snipe-it/storage/app/backups
cp -R snipe-it-backup/.env snipe-it/
cp -R snipe-it-backup/storage/oauth-private.key snipe-it/storage/oauth-private.key
cp -R snipe-it-backup/storage/oauth-public.key snipe-it/storage/oauth-public.key

php upgrade.php