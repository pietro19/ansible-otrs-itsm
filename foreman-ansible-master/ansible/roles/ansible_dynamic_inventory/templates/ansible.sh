export FOREMAN_INI_PATH=/etc/ansible/foreman.ini
export FOREMAN_URL="{{ ansible_default_ipv4.address }}"
export foreman_url="{{ ansible_default_ipv4.address }}"
export ANSIBLE_LOCAL_TEMP=~/.ansible/tmp