# --
# Copyright (C) 2016 ArtyCo (Artjoms Petrovs), http://artjoms.lv/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::GenericInterface::Operation::LinkObject::LinkAdd;

use strict;
use warnings;

use Kernel::System::ObjectManager;
use Kernel::System::LinkObject;
use Kernel::System::LinkObject::ITSMConfigItem;
use Kernel::System::VariableCheck qw(IsStringWithData IsHashRefWithData);
use parent qw(
    Kernel::GenericInterface::Operation::Common
);

our $ObjectManagerDisabled = 1;

=head1 NAME

Kernel::GenericInterface::Operation::LinkObject::LinkAdd - GenericInterface Link Create Operation backend

=head1 PUBLIC INTERFACE

=head2 new()

usually, you want to create an instance of this 
by using Kernel::GenericInterface::Operation->new();


=cut

sub new {
    my ( $Type, %Param ) = @_;

    my $Self = {};
    bless( $Self, $Type );

    # check needed objects
    for my $Needed (qw( DebuggerObject WebserviceID )) {
        if ( !$Param{$Needed} ) {
            return {
                Success      => 0,
                ErrorMessage => "Got no $Needed!"
            };
        }

        $Self->{$Needed} = $Param{$Needed};
    }

    # create additional objects
    local $Kernel::OM = Kernel::System::ObjectManager->new();
    $Self->{LinkObject} = $Kernel::OM->Get('Kernel::System::LinkObject');
    
    return $Self;
}

=head2 Run()

Create a new link.

    my $Result = $OperationObject->Run(
        Data => {
            SourceObject => 'Ticket',
            SourceKey    => '321',
            TargetObject => 'Ticket',
            TargetKey    => '12345',
            Type         => 'ParentChild',
            State        => 'Valid',
            UserID       => 1,
        },
    );

    $Result = {
        Success      => 1,                                # 0 or 1
        ErrorMessage => '',                               # In case of an error
        Data         => {
            Result => 1,                                  # 0 or 1 
        },
    };

=cut

sub Run {
    my ( $Self, %Param ) = @_;

    #check needed stuff
    # if ( !($Param->{UserLogin}) )
    # {
    #     return $Self->ReturnError(
    #         ErrorCode    => 'LinkAdd.MissingParameter',
    #         ErrorMessage => "LinkAdd: UserLogin is required!",
    #     );
    # }

    # if (( $Param{Data}->{UserLogin}) AND (!$Param{Data}->{Password}) )
    #     {
    #         return $Self->ReturnError(
    #             ErrorCode    => 'LinkAdd.MissingParameter',
    #             ErrorMessage => "LinkAdd: Password is required!",
    #         );
    #     }
    # }

    # authenticate user
    my ( $UserIDD, $UserType ) = $Self->Auth(
        %Param,
    );

    if ( !$UserIDD ) {
        return $Self->ReturnError(
            ErrorCode    => 'LinkAdd.AuthFail',
            ErrorMessage => "LinkAdd: User could not be authenticated!",
        );
    }

    # my $PermissionUserID = $UserID;
    # if ( $UserType eq 'Customer' ) {
    #     $UserID = $Kernel::OM->Get('Kernel::Config')->Get('CustomerPanelUserID')
    # }

    # # check needed hashes
    # if ( !IsHashRefWithData( $Param{Data} ) ) {
    #     return $Self->ReturnError(
    #         ErrorCode    => 'LinkAdd.MissingParameter',
    #         ErrorMessage => "LinkAdd: The request is empty!",
    #     );
    # }


    my $LinkID = $Self->{LinkObject}->LinkAdd(
        'SourceKey' => $Param{Data}{SourceKey},
        'SourceObject' => $Param{Data}{SourceObject},
        'State' => $Param{Data}{State},
        'TargetKey' => $Param{Data}{TargetKey},
        'TargetObject' => $Param{Data}{TargetObject},
        'Type' => $Param{Data}{Type},
        'UserID' => $Param{Data}{UserID},
    );

    if ( !$LinkID ) {
        return $Self->ReturnError(
            ErrorCode    => 'LinkAdd.AuthFail',
            ErrorMessage => "LinkAdd: Authorization failing!",
        );
    }

    return {
        Success => 1,
        Data    => {
            Result => $LinkID,
        },
    };
}

sub ReturnError {
    my ( $Self, %Param ) = @_;

    $Self->{DebuggerObject}->Error(
        Summary => $Param{ErrorCode},
        Data    => $Param{ErrorMessage},
    );

    # return structure
    return {
        Success      => 1,
        ErrorMessage => "$Param{ErrorCode}: $Param{ErrorMessage}",
        Data         => {
            Error => {
                ErrorCode    => $Param{ErrorCode},
                ErrorMessage => $Param{ErrorMessage},
            },
        },
    };
}

1;

=back

=head1 TERMS AND CONDITIONS

This software is part of the OTRS project (L<http://otrs.org/>).

This software comes with ABSOLUTELY NO WARRANTY. For details, see
the enclosed file COPYING for license information (AGPL). If you
did not receive this file, see L<http://www.gnu.org/licenses/agpl.txt>.

=cut