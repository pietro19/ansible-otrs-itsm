import requests
from requests.auth import HTTPDigestAuth
import json

url = "http://localhost/otrs/nph-genericinterface.pl/Webservice/TestTest/"
url_ticket="%sNewTicket" % url
url_link = "%sNewLink" % url
url_search = "%sConfigItemSearch" % url
url_ticket
user = "root@localhost"
pwd = "asdfgh"

# Funziona
payload1 = {
	"UserLogin": user,
	"Password": pwd,
    "SourceObject": 'Ticket',
    "SourceKey": 14,
    "TargetObject": 'Ticket',
    "TargetKey": 15,
    "Type": 'Normal',
    "State": 'Valid',
    "UserID": '1'
    }

requests.post(url_link, json=payload1).text

myResponse1 = requests.post(url_link, json=payload1)
myResponse1.text

# Funziona
payload= {
	"UserLogin": user,
	"Password": pwd,
	"Ticket": {
	    "Type": "Unclassified",
	    "Title": "some title",
	    "CustomerUser": "utente@prova.com",
	    "Queue": "Raw",
	    "State": "New",
	    "PriorityID": "3",
	    "MimeType": "text/plain"
		},
	"Article": {
		"Subject": "some subject",
	    "Body": "some body",
	    "Charset": "utf8",
	    "MimeType": "text/plain"
	}
}

myResponse = requests.post(url_ticket, json=payload)
myResponse.text
response_native = json.loads(myResponse.text)
response_native['TicketID']

url_link = "http://localhost/otrs/nph-genericinterface.pl/Webservice/TestTest/NewLink"

# Funziona
payload2 = {
	"UserLogin": user,
	"Password": pwd,
	"SourceObject": 'Ticket',
	"SourceKey": 14,
	"TargetObject": 'Ticket',
	"TargetKey": 13,
	"Type": 'ParentChild',
	"State": 'Valid',
	"UserID": 1
	}

myResponse2 = requests.post(url_link, json=payload2)
myResponse2.text


# Funziona
payload2 = {
	"UserLogin": user,
	"Password": pwd,
	"SourceObject": 'Ticket',
	"SourceKey": 14,
	"TargetObject": 'Ticket',
	"TargetKey": 15,
	"Type": 'Normal',
	"State": 'Valid',
	"UserID": 1
	}

myResponse2 = requests.post(url_link, json=payload2)
myResponse2.text

# Funziona
payload3 = {
	"UserLogin": user,
	"Password": pwd,
    "SourceObject": 'Ticket',
    "SourceKey": 14,
    "TargetObject": 'ITSMConfigItem',
    "TargetKey": 495,
    "Type": 'DependsOn',
    "State": 'Valid',
    "UserID": '1'
}

{'SourceObject': 'Ticket', 'State': 'Valid', 'TargetKey': "[u'496']", 'UserID': 1, 'UserLogin': 'root@localhost', 'Password': 'asdfgh', 'Type': 'DependsOn', 'SourceKey': u'28', 'TargetObject': 'ITSMConfigItem'}

myResponse3 = requests.post(url_link, json=payload3)
myResponse3.text

# Non deve funzionare: mancano user & pwd
payload4 = {
	"SourceObject": 'Ticket',
	"SourceKey": 14,
	"TargetObject": 'Ticket',
	"TargetKey": 15,
	"Type": 'Normal',
	"State": 'Valid',
	"UserID": '1'
	}

myResponse4 = requests.post(url_link, json=payload4)
myResponse4.text

33135000418

import requests
from requests.auth import HTTPDigestAuth
import json

url = "http://localhost/otrs/nph-genericinterface.pl/Webservice/TestTest/"
url_ticket="%sNewTicket" % url
url_link = "%sNewLink" % url
url_search = "%sConfigItemSearch" % url
url_ticket
user = "root@localhost"
pwd = "asdfgh"

# Funziona
payload5 = {
	"UserLogin": user,
	"Password": pwd,
	"ConfigItem":{
		"Number": 33135000418,
		"Class": "Virtual Machine"
	}
}

payload5 = {
	"UserLogin": user,
	"Password": pwd,
	"ConfigItem":{
		"Name": "donpotato.ovirt.iks.lab",
		"Class": "Virtual Machine"
	}
}

payload5 = {
	"UserLogin": user,
	"Password": pwd,
	"ConfigItem":{
		"Name": "donpotato2.ovirt.iks.lab",
		"Class": "Virtual Machine"
	}
}
myResponse5 = requests.get(url_search, json=payload5)
myResponse5.text
(json.loads(myResponse5.text))["ConfigItemIDs"]



# Da Capomastro:
url = "http://x-otrs-cmdb.iks.local/otrs/nph-genericinterface.pl/Webservice/TestTest/"
url_create = "%sConfigItemCreate" % url
user = "root@localhost"
pwd = "asdfgh"

payload_create = {
	"UserLogin": user,
	"Password": pwd,
	"ConfigItem":{
		"Class": "Virtual Machine",
		"Name": "donpotato.ovirt.iks.lab",
		"DeplState": "Production",
		"InciState": "Operational",
		"CIXMLData":{
			"FQDN": "donpotato.ovirt.iks.lab",
			"IPv4Address": "192.168.141.192"
		}
	}
}

myResponse_create = requests.post(url_create, json=payload_create)
myResponse_create.text

