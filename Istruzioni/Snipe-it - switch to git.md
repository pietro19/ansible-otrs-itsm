1. Make sure you have Git installed
2. Backup your old install

	php artisan snipeit:backup
3. Rename your Snipe-IT project root folder

	mv snipe-it-3.6.2 snipe-it-backup
4. Install via Git

	git clone https://github.com/snipe/snipe-it snipe-it-git
5. Run composer install
	
	mv snipe-it-git snipe-it
	cd snipe-it
	composer install --no-dev --prefer-source

6. Copy over the necessary old files

	cp -R snipe-it-backup/public/uploads/* snipe-it/public/uploads
	cp -R snipe-it-backup/storage/private_uploads/* snipe-it/storage/private_uploads
	cp -R snipe-it-backup/storage/app/backups/* snipe-it/storage/app/backups
	cp -R snipe-it-backup/.env snipe-it/
	cp -R snipe-it-backup/storage/oauth-private.key snipe-it/storage/oauth-private.key
	cp -R snipe-it-backup/storage/oauth-public.key snipe-it/storage/oauth-public.key
(If it asks you if you want to overwrite .gitignore or .gitkeep, say no.)

Now when you want to upgrade Snipe-IT, you can simply run [not as root]
	
	php upgrade.php
For example:
	su -c 'php upgrade.php' -s /bin/bash apach
eThe command wasn't working on CentOS7, ref. https://github.com/snipe/snipe-it/issues/4726. The solution proposed was the installation of the package php71u-process (yum install php71u-process).
After that, the command worked.