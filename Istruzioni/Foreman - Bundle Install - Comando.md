Foreman sconsiglia di iniziare l'installazione come root, perché potrebbe rompere l'installazione per gli altri utenti di Foreman; suggerisce, invece, di iniziarla come l'utente *foreman*: il comando per far ciò è il seguente:

	su -c 'bundle install' -s /bin/bash foreman
