results = {
"ansible_devices::dm-0": "",
"ansible_devices::dm-0::holders": "[]",
"ansible_devices::dm-0::host": "",
"ansible_devices::dm-0::links": "",
"ansible_devices::dm-0::links::ids": "[\"dm-name-centos_snipeit-root\", \"dm-uuid-LVM-VJBPHNo8TDP5u63vdj7PvzMw2FLMDBVfMPo0DyoeXHedn7gfSrfpVX645oXo1OU2\"]",
"ansible_devices::dm-0::links::labels": "[]",
"ansible_devices::dm-0::links::masters": "[]",
"ansible_devices::dm-0::links::uuids": "[\"a72ee3ff-5e1c-4ee8-aa6f-c7ee29459051\"]",
"ansible_devices::dm-0::model": "",
"ansible_devices::dm-0::partitions": "",
"ansible_devices::dm-0::removable": "0",
"ansible_devices::dm-0::rotational": "1",
"ansible_devices::dm-0::sas_address": "",
"ansible_devices::dm-0::sas_device_handle": "",
"ansible_devices::dm-0::scheduler_mode": "",
"ansible_devices::dm-0::sectorsize": "512",
"ansible_devices::dm-0::size": "26.47 GB",
"ansible_devices::dm-0::support_discard": "0",
"ansible_devices::dm-0::vendor": "",
"ansible_devices::dm-0::virtual": "1",
"ansible_devices::dm-1": "",
"ansible_devices::dm-1::holders": "[]",
"ansible_devices::dm-1::host": "",
"ansible_devices::dm-1::links": "",
"ansible_devices::dm-1::links::ids": "[\"dm-name-centos_snipeit-swap\", \"dm-uuid-LVM-VJBPHNo8TDP5u63vdj7PvzMw2FLMDBVfo460SKEGcGWiKbxwkHco6rR8hjszRW0Y\"]",
"ansible_devices::dm-1::links::labels": "[]",
"ansible_devices::dm-1::links::masters": "[]",
"ansible_devices::dm-1::links::uuids": "[\"6ade565e-fc34-4391-b6a6-c89ea516c442\"]",
"ansible_devices::dm-1::model": "",
"ansible_devices::dm-1::partitions": "",
"ansible_devices::dm-1::removable": "0",
"ansible_devices::dm-1::rotational": "1",
"ansible_devices::dm-1::sas_address": "",
"ansible_devices::dm-1::sas_device_handle": "",
"ansible_devices::dm-1::scheduler_mode": "",
"ansible_devices::dm-1::size": "3.00 GB",
"ansible_devices::dm-1::support_discard": "0",
"ansible_devices::dm-1::vendor": "",
"ansible_devices::dm-1::virtual": "1"
}


b=[]
for key in results.keys():
    r=((re.search(r"ansible_devices\:\:([\w\-]*)$", key)))
    if r:
        b.append(r.group(1))

print(b)


