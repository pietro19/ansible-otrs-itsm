#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: otrs_import

short_description: Create a CSV for OTRS::ITSM

version_added: "2.4"

description:
    - "This module create a CSV file for OTRS::ITSM; the host params written match the mappings keys of the import/export template"

author:
    - Pietro Gabelli
'''

RETURN = '''
message:
    description: The data for *host* has been written in *file*
'''

import socket
from socket import AF_INET, SOCK_STREAM, SOCK_DGRAM

from ansible.plugins.callback import CallbackBase

import csv, operator
import json, os

import requests
import json

url = "http://192.168.141.7/api/v1/hardware"

my_api_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjM0YjY4ZmI0MDQyYzczMGE1ZDA5Y2E4OTcyMWMxNzEzMzMyN2E3YjBjYmRhZjBhY2Q0NGEwMTJjYTc4NWRkMzE4ZTU1MTUxMGFkNzg3M2QyIn0.eyJhdWQiOiIxIiwianRpIjoiMzRiNjhmYjQwNDJjNzMwYTVkMDljYTg5NzIxYzE3MTMzMzI3YTdiMGNiZGFmMGFjZDQ0YTAxMmNhNzg1ZGQzMThlNTUxNTEwYWQ3ODczZDIiLCJpYXQiOjE1MTM3MDA2NTksIm5iZiI6MTUxMzcwMDY1OSwiZXhwIjoxNTQ1MjM2NjU5LCJzdWIiOiIyNDYiLCJzY29wZXMiOltdfQ.eXjH2wyGXIIZYlnlPot5JUFfuSlaIhIJh1WGxdj7aDJwW8ZIga95UGJBMKpDWiS1x5oItTLINykdKCRoNQNartcJFNcyDSNFzl0MuWuNH6v5BoAKsAhNmeD7T9lEKsBQzW1JToWr37ik6ePBgovdk24-oltxMYVHc43sMILm-jwrSRTyxMoBK4BxWBNe3UjIgbxlP7QoaWwq1enyjOemKjRW3dSC-Oq4tD6m-Q6xbsza6AWklvk_NC_mFhcUVVElAATLBjwzLHYZ9svE4C_8KsqfgbewhFoqYCn1HLRIfS3q0mz1ofjg0gBDqfCPFIj803pBLZgm-JcQyTgPHPcr269eJhKDrZHRutknxtb7uYmv8voU9u_Gyzabt2RiVMBuiClECJwXOKwDS3KJbuiDNYCD_fO1FrRlyGOn9_Y4YOsMlU8BCxslWf_MvHk9qxu_A5Q-14HalHQcYvFQFs4xDcuzS48Tdt_97lL9OBiessZeRpvrNXJxk0pqkDiGwVcdfUGApuoUGEa-NGyizvxZb2PFis-CO3ykNSCXW4c2-yfDr_uwnfqqUQsVyzoUbHGTwSckj9sM_ZG7geIdxBxDkK4Q6tgxGgQA83zsDzvzKJ2Ava7pSjUpXSmbsvrjnvjkv_FnMgYzR-MMboWLLYN1U_iNLBGPFX5utB5PREnpkyI"

headers = {"Authorization":"Bearer "+my_api_token, "Accept":"application/json"}


def send_data(payload):
    response = requests.post(url, data=payload, headers=headers)
    response_native = json.loads(response.text)
    
    msg = response_native['messages']
    if type(response_native['messages']) is unicode:
        to_print="L'invio dei dati dell'host ha avuto come risposta: " + response_native['status'] + "; " + msg
    else:
        to_print="L'invio dei dati dell'host ha avuto come risposta: " + response_native['status'] + "; "
        for i in msg:
            to_add = i + ": " + msg[i][0]
            to_print += to_add

    print(to_print)

    with open('result', 'w') as outfile:
        outfile.write(response.text)


def extract_data(host, result_json):
    ''' Gather data on the host''' 
    category='Virtual Machine'
    #check if is not a VM
    if (result_json['ansible_virtualization_role'] == "host"):
        category = 'Server'
    host_info=[['name', host],
        ['category', category],
        ['model_id', 86],
        ['serial', result_json['ansible_product_serial']],
        ['status_id', 5],
        ['asset_tag', result_json['ansible_fqdn']],
        ['order_number', '']]

#  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
#  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
#  `asset_tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
#  `model_id` int(11) DEFAULT NULL,
#  `serial` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
#  `purchase_date` date DEFAULT NULL,
#  `purchase_cost` decimal(20,2) DEFAULT NULL,
#  `order_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
#  `assigned_to` int(11) DEFAULT NULL,
#  `notes` text COLLATE utf8_unicode_ci,
#  `image` text COLLATE utf8_unicode_ci,
#  `user_id` int(11) DEFAULT NULL,
#  `created_at` timestamp NULL DEFAULT NULL,
#  `updated_at` timestamp NULL DEFAULT NULL,
#  `physical` tinyint(1) NOT NULL DEFAULT '1',
#  `deleted_at` timestamp NULL DEFAULT NULL,
#  `status_id` int(11) DEFAULT NULL,
#  `archived` tinyint(1) DEFAULT NULL,
#  `warranty_months` int(11) DEFAULT NULL,
#  `depreciate` tinyint(1) DEFAULT NULL,
#  `supplier_id` int(11) DEFAULT NULL,
#  `requestable` tinyint(4) NOT NULL DEFAULT '0',
#  `rtd_location_id` int(11) DEFAULT NULL,
#  `_snipeit_mac_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
#  `accepted` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
#  `last_checkout` datetime DEFAULT NULL,
#  `expected_checkin` date DEFAULT NULL,
#  `company_id` int(10) unsigned DEFAULT NULL,

    return host_info


class CallbackModule(CallbackBase):
    CALLBACK_VERSION = 3.0
    CALLBACK_TYPE = 'aggregate'
    CALLBACK_NAME = 'hostlist'
    CALLBACK_NEEDS_WHITELIST = True

    def __init__(self):
        super(CallbackModule, self).__init__()

    def runner_on_ok(self, host, result):
        try:
            if result['ansible_facts']:
                host_info=extract_data(host, result['ansible_facts'])
                #print(host_info)
                with open("./host_data", "w") as file:
                    file.write(json.dumps(host_info))
                send_data(host_info)
        except:
            pass