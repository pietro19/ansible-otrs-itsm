# Passi #
1. Andare su [ovirt.iks.local/ovirt_engine/sso/login.html] -> Virtual Machine tab
2. Selezionando __New VM__ viene presentata la maschera con cui crearla
3. A questo punto si può scegliere il template tra quelli già pronti, oppure procedere con un'installazione personalizzata, definendo OS, tipo dell'istanza, ottimizzazione per Server/Desktop. Inserire il nome desiderato.
Si possono definire anche:
	* Dimensione HDD
	* Quantità di RAM
	* il numero di Cpu virtuali
	* La memoria fisica garantita, da mettere ad 1 MB (il minimo possibile)

4. Creata la macchina, aprire una console su root@services.ovirt.iks.lab
5. Si devono cambiare i due file usati per gestire gli indirizzi:
	* /etc/named/zones/db.ovirt.iks.lab
	* /etc/named/zones/db.192.168.141
Nel primo va aggiunto il nome del server con un punto alla fine, con una forma simile a:

	mioserver.ovirt.iks.lab.  IN              A       192.168.141.80
L'indirizzo va scelto tra quelli liberi, ma tenendo conto che sono scritti in ordine progressivo e che sono riportati solo quelli già assegnati, è immediato.

Nel secondo va inserito l'ultima n.upla di cifre dell'indirizzo scelto, nel nostro caso è 80. La forma è quindi:

	80      IN      PTR     mioserver.ovirt.iks.lab.
Anche qui gli indirizzi sono ordinati, si tratta quindi di conservare l'ordine del file.

Una volta concluse le modifiche, eseguire i comandi:
	
systemctl restart named
systemctl status named
7. Fatto questo, tornare su Ovirt alla macchina creata, e spostare l'hard disk sullo spazio più libero e rinominarlo con il nome della macchina ed il numero del disco, nel nostro caso: mioserver.ovirt.iks.lab_Disk1
8. Attivare la macchina: tasto "Run" in alto
9. Modificare l'indirizzo di rete, accedendo alla macchina, aprendo una shell su root@mioserver.ovirt.iks.lab.
Utilizzando nmtui, assegnare:
	- modificare la connessione:
		Profile name: eth0
		ipv4 Configuration:
			address: 192.168.141.80/24
			Gateway:		192.168.141.1
			DNS Servers:	192.168.141.2
			Search domains:	ovirt.iks.lab
							iks.local
		ipv6 Configuration <Automatic>
	- Impostare l'hostname:
		Hostname: mioserver.ovirt.iks.lab
Riavviare il server, tramite 
	
	reboot -h now
A questo punto è stata inserita la chiave pubblica per far collegare ovirt, con i comandi che seguono:

	mkdir -p .ssh
	cat << EOF >> .ssh/authorized_keys
	ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAgEA42K8fg6K1VaKsSqvBXPeg1os2X8s6jy4GSjHzX2uPpVIsB1X0ClYAx3MkReJJ7hIHuWZO0/Zm1s5JcjbGksIr2B/j/oeUGj7cL70x7U2/LfQ/jmxHEzTPfCtqww2vz/Yxcvx8qhAiDF3fw/tWLQQSucd/F3vDNNhZZu7l4CZb5tf0Ua8n27DGjeMxLvQTF5ACgF8CLQWs8R1d530WYpdSZWvj94ya4DevqZC6lqaQNgRFghvEm3wmybhfVWjanKf5KRNj06ySYIoUBizPDNta4Gzgf7BRNOhdusxg4sqo/Y9EW79R0ejBJxH2/emIDFgRALSzxGMZXTvIjE/aOdxlidndJm7r6gGDSYEbV8aKfLQhFxIqPieAECXajOjuGhT/cpeupBnfGXuhetAgRKXHBgRbpL3ROUrD+xA+cDXUjpUZ5RuyLJ4/DVxJjhv9LyvhBQu+qos+WhudU4FQR3uX/B/ioNpkr/xtMEPFr2JWeAF5heFK8rs61zItsCi+x3WHY0c1w+h2TFQjB6n1et/kIIeiOIXmQ/G5H0PufogU9Vl1St3bzaKK2Bg64YubMSbzFWuA9oTJ1IPGSiMJ/HP81Jez5IpXVopv0xQq8qgcWDNfX8jIQ9QwEBi/ZRjI/9tINBB2oBtdqRTt/13EvTtIoagcN9XO4Sz1mbbROONFU8= rsa-key-20150823-x-00398
	ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDWJgE/MNRgaVgiBgJf59MP1ejhWKO4TI/F+/ODzkTMjTm1t6hAvxMsDKti3XoHFDWlaxyVPYux33CDBWINa/en+E5Fkws+3QuntET8dfKNBHF0xmHzSsK2NlIRF2Dy/GyO3cLSlGHp6B3DaaQl9Qu5sYlPrlwRgMXV8V6lRmQuuf0h+ZlgvZzj2Kj6uiC0wbaIsMdMoI+Hiudc1QBDIm4/BVy7MQU/dPMgWESVG+SmZcjE/WLbs3AtKjSGVoB1Nf9Ir3ltL56g5ksn9vff/gFsWOyrFJCRG61f5PoNieop9IrbDlPrLqD2JQov8bCmX6fKQH5vt6C2EYfnVPasT3Fx root@services.ovirt.iks.lab
	EOF
	chmod og-rwx -R .ssh




capomastro.ovirt.iks.lab.  IN              A       192.168.141.81
capomastro-new.ovirt.iks.lab.  IN              A       192.168.141.82