# Ansible role per importare i dati dell'host in OTRS::ITSM #

L'import si basa su:

1. Recupero dei dati dell'host e preparazione del file CSV per l'host specifico
2. Scrittura del csv file nella macchina dove gira OTRS
3. Lancio dello script che esegue l'import
