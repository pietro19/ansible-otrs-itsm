[root@snipeit snipe-it-4.1.8]# php artisan --help
Usage:
  list [options] [--] [<namespace>]

Arguments:
  namespace            The namespace name

Options:
      --raw            To output raw command list
      --format=FORMAT  The output format (txt, xml, json, or md) [default: "txt"]

Help:
  The list command lists all commands:

    php artisan list

  You can also display the commands for a specific namespace:

    php artisan list test

  You can also output the information in other formats by using the --format option:

    php artisan list --format=xml

  It's also possible to get raw list of commands (useful for embedding command runner):

    php artisan list --raw
[root@snipeit snipe-it-4.1.8]# php artisan list
Laravel Framework 5.4.35

Usage:
  command [options] [arguments]

Options:
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
      --env[=ENV]       The environment the command should run under
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Available commands:
  clear-compiled                Remove the compiled class file
  down                          Put the application into maintenance mode
  env                           Display the current framework environment
  help                          Displays help for a command
  inspire                       Display an inspiring quote
  list                          Lists commands
  migrate                       Run the database migrations
  optimize                      Optimize the framework for better performance
  serve                         Serve the application on the PHP development server
  tinker                        Interact with your application
  up                            Bring the application out of maintenance mode
 app
  app:name                      Set the application namespace
 auth
  auth:clear-resets             Flush expired password reset tokens
 backup
  backup:clean                  Remove all backups older than specified number of days in config.
  backup:list                   Display a list of all backups.
  backup:monitor                Monitor the health of all backups.
  backup:run                    Run the backup.
 cache
  cache:clear                   Flush the application cache
  cache:forget                  Remove an item from the cache
  cache:table                   Create a migration for the cache database table
 config
  config:cache                  Create a cache file for faster configuration loading
  config:clear                  Remove the configuration cache file
 db
  db:seed                       Seed the database with records
 debugbar
  debugbar:clear                Clear the Debugbar Storage
 event
  event:generate                Generate the missing events and listeners based on registration
 key
  key:generate                  Set the application key
 make
  make:auth                     Scaffold basic login and registration views and routes
  make:command                  Create a new Artisan command
  make:controller               Create a new controller class
  make:event                    Create a new event class
  make:job                      Create a new job class
  make:listener                 Create a new event listener class
  make:mail                     Create a new email class
  make:middleware               Create a new middleware class
  make:migration                Create a new migration file
  make:model                    Create a new Eloquent model class
  make:notification             Create a new notification class
  make:policy                   Create a new policy class
  make:provider                 Create a new service provider class
  make:request                  Create a new form request class
  make:seeder                   Create a new seeder class
  make:test                     Create a new test class
 migrate
  migrate:install               Create the migration repository
  migrate:refresh               Reset and re-run all migrations
  migrate:reset                 Rollback all database migrations
  migrate:rollback              Rollback the last database migration
  migrate:status                Show the status of each migration
 notifications
  notifications:table           Create a migration for the notifications table
 passport
  passport:client               Create a client for issuing access tokens
  passport:install              Run the commands necessary to prepare Passport for use
  passport:keys                 Create the encryption keys for API authentication
 queue
  queue:failed                  List all of the failed queue jobs
  queue:failed-table            Create a migration for the failed queue jobs database table
  queue:flush                   Flush all of the failed queue jobs
  queue:forget                  Delete a failed queue job
  queue:listen                  Listen to a given queue
  queue:restart                 Restart queue worker daemons after their current job
  queue:retry                   Retry a failed queue job
  queue:table                   Create a migration for the queue jobs database table
  queue:work                    Start processing jobs on the queue as a daemon
 route
  route:cache                   Create a route cache file for faster route registration
  route:clear                   Remove the route cache file
  route:list                    List all registered routes
 schedule
  schedule:run                  Run the scheduled commands
 session
  session:table                 Create a migration for the session database table
 snipeit
  snipeit:backup                This command creates a database dump and zips up all of the uploaded files in the upload directories.
  snipeit:create-admin          Create an admin user via command line.
  snipeit:demo-settings         This will reset the Snipe-IT demo settings back to default.
  snipeit:expected-checkin      Check for overdue or upcoming expected checkins.
  snipeit:expiring-alerts       Check for expiring warrantees and service agreements, and sends out an alert email.
  snipeit:import                Import Items from CSV
  snipeit:inventory-alerts      This command checks for low inventory, and sends out an alert email.
  snipeit:ldap-disable          This is  a rescue command that can be used to turn off LDAP settings in the event that you managed to lock yourself out using bad LDAP settings.
  snipeit:ldap-sync             Command line LDAP sync
  snipeit:legacy-recrypt        This command allows upgrading users to de-encrypt their deprecated mcrypt encrypted fields and re-encrypt them using the current OpenSSL encryption.
  snipeit:pave                  Pave the database to start over. This should ALMOST NEVER BE USED. (It is primarily a quick tool for developers.)
  snipeit:purge                 Purge all soft-deleted deleted records in the database. This will rewrite history for items that have been edited, or checked in or out. It will also reqrite history for users associated with deleted items.
  snipeit:regenerate-tags       This utility will regenerate all asset tags. THIS IS DATA-DESTRUCTIVE AND SHOULD BE USED WITH CAUTION.
  snipeit:sync-asset-locations  This utility will sync the location_id of assets based on current state. It should not normally be needed, but is a safeguard in case we missed something in the Great Migration when flattening the assets to location relationship.
  snipeit:travisci-install      Travis-cli install script for unit tests
  snipeit:unescape              This should be run to fix some double-escaping issues from earlier versions of Snipe-IT.
 storage
  storage:link                  Create a symbolic link from "public/storage" to "storage/app/public"
 vendor
  vendor:publish                Publish any publishable assets from vendor packages
 version
  version:update                Command description
 view
  view:clear                    Clear all compiled view files
