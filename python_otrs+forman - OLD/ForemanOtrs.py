# -*- coding: utf-8 -*-
from __future__ import print_function
try:
    # Python 2 version
    import ConfigParser
except ImportError:
    # Python 3 version
    import configparser as ConfigParser
import json, argparse, copy, os, re, sys, requests, datetime, time, csv, operator, difflib

from time import time
from collections import defaultdict
from distutils.version import LooseVersion, StrictVersion
from requests.auth import HTTPBasicAuth
from subprocess import call

if LooseVersion(requests.__version__) < LooseVersion('1.1.0'):
    print('This script requires python-requests 1.1 as a minimum version')
    sys.exit(1)

PATH = './'
FILE_NAME = 'Ansible_Import_Hosts.csv'
FILE_NAME_VM = 'Ansible_Import_VM_Hosts.csv'

def json_format_dict(data, pretty=False):
    """Converts a dict to a JSON object and dumps it as a formatted string"""

    if pretty:
        return json.dumps(data, sort_keys=True, indent=2)
    else:
        return json.dumps(data)

class ForemanInventory(object):
#Creazione array di facts  degli host di Foremna
    def __init__(self):
        self.facts = dict()   # Facts of each host
        self.session = None   # Requests session
        self.config_paths = [
            "./ForemanOtrs.ini",
            os.path.dirname(os.path.realpath(__file__)) + '/ForemanOtrs.ini',
        ]

    def read_settings(self):
        """Reads the settings from the ForemanOtrs.ini file"""

        config = ConfigParser.SafeConfigParser()
        config.read(self.config_paths)

        # Foreman API related
        try:
            self.foreman_url = config.get('foreman', 'url')
            self.foreman_user = config.get('foreman', 'user')
            self.foreman_pw = config.get('foreman', 'password')
            self.foreman_ssl_verify = config.getboolean('foreman', 'ssl_verify')
        except (ConfigParser.NoOptionError, ConfigParser.NoSectionError) as e:
            print("Error parsing configuration: %s" % e, file=sys.stderr)
            return False

        return True

    def parse_cli_args(self):
        """Command line argument processing"""

        parser = argparse.ArgumentParser(description='Produce an Ansible Inventory file based on foreman')
        parser.add_argument('--list', action='store_true', default=True, help='List instances (default: True)')
        parser.add_argument('--host', action='store', help='Get all the variables about a specific instance')
        # parser.add_argument('--refresh-cache', action='store_true', default=False,
        #                     help='Force refresh of cache by making API requests to foreman (default: False - use cache files)')
        self.args = parser.parse_args()

    def _get_session(self):
        if not self.session:
            self.session = requests.session()
            self.session.auth = HTTPBasicAuth(self.foreman_user, self.foreman_pw)
            self.session.verify = self.foreman_ssl_verify
        return self.session

    def _get_json(self, url, ignore_errors=None, params=None):
        if params is None:
            params = {}
        params['per_page'] = 250

        page = 1
        results = []
        s = self._get_session()
        while True:
            params['page'] = page
            ret = s.get(url, params=params)
            if ignore_errors and ret.status_code in ignore_errors:
                break
            ret.raise_for_status()
            json = ret.json()
            # /hosts/:id has not results key
            if 'results' not in json:
                return json
            # Facts are returned as dict in results not list
            if isinstance(json['results'], dict):
                return json['results']
            # List of all hosts is returned paginaged
            results = results + json['results']
            if len(results) >= json['subtotal']:
                break
            page += 1
            if len(json['results']) == 0:
                print("Did not make any progress during loop. "
                      "expected %d got %d" % (json['total'], len(results)),
                      file=sys.stderr)
                break
        return results

    def _get_hosts(self):
        url = "%s/api/v2/hosts" % self.foreman_url

        params = {}

        return self._get_json(url, params=params)

    def _get_host_data_by_id(self, hid):
        url = "%s/api/v2/hosts/%s" % (self.foreman_url, hid)
        return self._get_json(url)

    def _get_facts_by_id(self, hid):
        url = "%s/api/v2/hosts/%s/facts" % (self.foreman_url, hid)
        return self._get_json(url)

    def _get_facts(self, host):
        """Fetch all host facts of the host"""

        ret = self._get_facts_by_id(host['id'])
        if len(ret.values()) == 0:
            facts = {}
        elif len(ret.values()) == 1:
            facts = list(ret.values())[0]
        else:
            raise ValueError("More than one set of facts returned for '%s'" % host)
        return facts
    
    def write_to_cache(self, data, filename):
        """Write data in JSON format to a file"""
        json_data = json_format_dict(data, True)
        cache = open(filename, 'w')
        cache.write(json_data)
        cache.close()

    def get_inventory(self, scan_only_new_hosts=False):

        self.groups = dict()
        self.hosts = dict()
        
        for host in self._get_hosts():
            dns_name = host['name']
            
            self.facts[dns_name] = self._get_facts(host)
            
            host_data =self._get_host_data_by_id(host['id'])
            myvar=self.facts[dns_name]
            myvar.update(host_data)
            self.extract_data(dns_name, myvar)

#Creazione e valorizzazione file per OTRS
    def create_report_file(self, file):
        os.chdir(PATH)
        open(file, 'a').close()

    def remove_file(self, file):
        os.chdir(PATH)
        if(os.path.exists(file)):
        	os.remove(file)

    def write_data(self, file, row):
        os.chdir(PATH)
        with open(file) as f:
            with open("tmp-%s" % (file), "w") as csvfile:
                for line in f:
                    csvfile.write(line)
                writer = csv.writer(
                    csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
                writer.writerow(row)

    def rename_file(self, file):
        os.chdir(PATH)
        os.remove(file)
        os.rename("tmp-%s" % (file), file)

    def clean_string(self, to_clean):
        for ch in ['\"','[', ']']:
            if ch in to_clean:
                to_clean = to_clean.replace(ch, '')
        return to_clean

    def extract_data(self, host, result_json):
        name=host
        serial_no = ""
        fqdn = host
        opsys = result_json['operatingsystem_name']
        installed_sw = ""
        cpu_name = ""
        cpu_number = ""
        cpu_cores = ""
        ram = ""
        hard_disks = []
        n_hdd = 0

        ipv4 = result_json['ip']
        ipv6 = result_json['ip6']
        if 'ansible_product_serial' in result_json:
            serial_no = result_json['ansible_product_serial']
        installed_sw = ""
        if 'ansible_processor' in result_json:
            cpu_data = self.clean_string(result_json['ansible_processor'])
            cpu_name= max(cpu_data.split(','), key=len)
        if 'ansible_processor_count' in result_json:
            cpu_number = result_json['ansible_processor_count']
        if 'ansible_processor_cores' in result_json:
            cpu_cores = result_json['ansible_processor_cores']
        if 'ansible_memory_mb::real::total' in result_json:
            ram_prov = result_json['ansible_memory_mb::real::total']
            ram = str(ram_prov)+" MB"
        if 'ansible_devices' in result_json:
            b=[]
            for key in result_json.keys():
                r = ((re.search(r"ansible_devices\:\:([\w\-]*)$", key)))
                if r:
                    b.append(r.group(1))
            for hd in b:
                size = ''
                hdd_name = ''
                if 'ansible_devices::' + hd + '::size' in result_json:
                    size = result_json['ansible_devices::' + hd + '::size']
                if 'ansible_devices::' + hd + '::model' in result_json:
                    hdd_name = result_json['ansible_devices::' + hd + '::model']
                elif 'ansible_devices::' + hd + '::links::ids' in result_json:
                        hdd_name = self.clean_string(result_json['ansible_devices::' + hd + '::links::ids'].split(',')[0])
                if n_hdd<10:
                    hard_disks.extend([str(hdd_name), str(size)])
                    n_hdd+=1
        for i in xrange(n_hdd, 10):
            hard_disks.extend(["", ""])
        exposed_srvcs = ""
        row = [name, "Production", "Operational", serial_no, fqdn, opsys, installed_sw, cpu_name, cpu_number, cpu_cores, ram]
        row.extend(hard_disks)
        row.extend([ipv4, ipv6, exposed_srvcs])

        uni_row=map(unicode,row)

        file = FILE_NAME_VM

        if hasattr(result_json, 'ansible_virtualization_role'):
            virt_role = result_json['ansible_virtualization_role']
            file = FILE_NAME if (virt_role == "host") else FILE_NAME_VM
        
        os.chdir(PATH)
                
        if not os.path.exists(file):
            self.create_report_file(file)
        self.write_data(file, uni_row)
        self.rename_file(file)

#parte run
    def run(self):
        # Read settings and parse CLI arguments
        if not self.read_settings():
            return False
        self.remove_file(FILE_NAME)
        self.remove_file(FILE_NAME_VM)
        self.parse_cli_args()
        self.get_inventory()
        return True

ForemanInventory().run()

# su -c 'perl /opt/otrs/bin/otrs.Console.pl Admin::ITSM::ImportExport::Export --template-number 000003 /opt/otrs/CMDB_Import/export.csv' -s /bin/bash otrs

class OTRSImport(object):
    #OTRS templates:
        #000006 -> VM
        #000005 -> Computers
    #export otrs file

    def __init__(self):
        self.config_paths = [
            "./ForemanOtrs.ini",
            os.path.dirname(os.path.realpath(__file__)) + '/ForemanOtrs.ini',
        ]

    def read_settings(self):
        """Reads the settings from the ForemanOtrs.ini file"""

        config = ConfigParser.SafeConfigParser()
        config.read(self.config_paths)

        # OTRS API related
        try:
            self.otrs_base_url = config.get('otrs', 'url')
            self.otrs_new_ticket = config.get('otrs', 'new_ticket_method')
            self.otrs_new_link_method = config.get('otrs', 'new_link_method')
            self.otrs_search_ci_method = config.get('otrs', 'search_ci_method')
            self.otrs_user = config.get('otrs', 'user')
            self.otrs_pw = config.get('otrs', 'password')
            self.otrs_user_id = config.get('otrs', 'user_id')
        except (ConfigParser.NoOptionError, ConfigParser.NoSectionError) as e:
            print("Error parsing configuration: %s" % e, file=sys.stderr)
            return False

        print("terminata lettura valori in ingresso")
        return True


    def sync_otrs(self):
        # Credenziali per OTRS

        url_ticket = "%s%s" % (self.otrs_base_url, self.otrs_new_ticket)
        url_link = "%s%s" % (self.otrs_base_url, self.otrs_new_link_method) 
        url_search = "%s%s" % (self.otrs_base_url, self.otrs_search_ci_method)

        user = self.otrs_user
        pwd = self.otrs_pw
        user_id = self.otrs_user_id
        if(os.path.exists(FILE_NAME_VM)): #sono state create delle VM
            #call command to update OTRS DB
            call("su -c 'perl /opt/otrs/bin/otrs.Console.pl Admin::ITSM::ImportExport::Import --template-number 000006 ./" + FILE_NAME_VM + "' -s /bin/bash otrs", shell=1)

            call("su -c 'perl /opt/otrs/bin/otrs.Console.pl Admin::ITSM::ImportExport::Export --template-number 000006 /tmp/export-old.csv' -s /bin/bash otrs", shell=1)

            otrs_hosts=[]
            with open('/tmp/export-old.csv', 'rb') as f:
                reader = csv.reader(f, delimiter=';')
                otrs_hosts = list(reader)
            
            foreman_hosts=[]
            with open(FILE_NAME_VM, 'rb') as f:
                reader = csv.reader(f, delimiter=';')
                foreman_hosts = list(reader)
           
            old_fqdn=[]
            new_fqdn=[]
            for items in otrs_hosts:
                old_fqdn.append(items[4])
            
            for items_new in foreman_hosts:
                new_fqdn.append(items_new[4])
            # diff2 = difflib.unified_diff(old_fqdn, new_fqdn, lineterm='', n=0)
            # lines2 = list(diff2)[2:]
            # removed = [line[1:] for line in lines2 if line[0] == '-']

            removed = [item for item in old_fqdn if item not in new_fqdn]
    
            print(removed)

            call("su -c 'perl /opt/otrs/bin/otrs.Console.pl Admin::ITSM::ImportExport::Export --template-number 000007 /tmp/export-ids.csv' -s /bin/bash otrs", shell=1)
            otrs_ids = dict()
            with open('/tmp/export-ids.csv', 'rb') as f:
                otrs_ids = dict(filter(None, csv.reader(f, delimiter=';')))

            for host in removed:
                payload_search = {
                    "UserLogin": user,
                    "Password": pwd,
                    "ConfigItem":{
                        "Number": otrs_ids[host],
                        "Class": "Virtual Machine"
                    }
                }
                response_search = requests.get(url_search, json=payload_search)
                native_response_search = json.loads(response_search.text)
                if "ConfigItemIDs" in native_response_search:
                    host_id = str(native_response_search["ConfigItemIDs"][0])
                    # print(host_id)

                    # apri il ticket
                    payload_ticket = {
                        "UserLogin": user,
                        "Password": pwd,
                        "Ticket": {
                            "Type": "Unclassified",
                            "Title": "VM indisponibile",
                            "CustomerUser": "utente@prova.com",
                            "Queue": "Postmaster",
                            "State": "New",
                            "PriorityID": "3",
                            "MimeType": "text/plain"
                            },
                        "Article": {
                            "Subject": "Ticket Automatico",
                            "Body": "Non è stato possibile trovare la macchina nell'elenco degli host. Potrebbe essere stata eliminata",
                            "Charset": "utf8",
                            "MimeType": "text/plain"
                        }
                    }

                    response_ticket = (requests.post(url_ticket, json=payload_ticket).text)
                    native_response_ticket = json.loads(response_ticket)
                    # print(native_response_ticket)
                    if 'TicketID' in native_response_ticket:
                        ticket_id = str(native_response_ticket['TicketID'])
                        # print(ticket_id)
                        # collegalo al CI
                        payload_link = {
                            "UserLogin": user,
                            "Password": pwd,
                            "SourceObject": 'Ticket',
                            "SourceKey": ticket_id,
                            "TargetObject": 'ITSMConfigItem',
                            "TargetKey": host_id,
                            "Type": 'DependsOn',
                            "State": 'Valid',
                            "UserID": user_id
                            }
                        
                        # print(payload_link)
                        myResponse = requests.post(url_link, json=payload_link)

                        if myResponse.status_code == 200:
                            to_print = "Aperto ticket %s su VM %s" % (ticket_id, host_id)
                            print(to_print)


    def run(self):
        if not self.read_settings():
            return False

        self.sync_otrs()
        return True

OTRSImport().run()