# Foreman machine setup: #
Su questa macchina sono stati installati Foreman ed Ansible; l'obiettivo era d'avere Foreman+Ansible che riescono a parlare con Ovirt, Snipe-IT ed OTRS per creare VM e comunicare i facts.
L'obiettivo finale è avere Foreman che riesce a fare tutte queste operazioni senza utilizzare Puppet.

##Sept 1. Installazione Ansible##

	sudo yum install epel-release
	sudo yum install ansible


Configurazione variabili d'ambiente -> file: /etc/profile.d/ansible.sh
Contenuto:

	#The URL of your Foreman installation (default “http://localhost:3000”)
	- FOREMAN_URL: https://foreman.ovirt.iks.lab/ 
	#The public key when using SSL client certificates (default “/etc/foreman/client_cert.pem”)
	#- FOREMAN_SSL_CERT: 
	#The private key when using SSL client certificates (default “/etc/foreman/client_key.pem”)
	#- FOREMAN_SSL_KEY: 
	#Wether to verify SSL certificates. Use False to disable certificate checks. You can also set it to CA bundle (default is “True”).
	- FOREMAN_SSL_VERIFY: False

	export FOREMAN_URL=https://foreman.ovirt.iks.lab
	export FOREMAN_SSL_CERT=/etc/puppetlabs/puppet/ssl/certs/foreman.ovirt.iks.lab.pem
	export FOREMAN_SSL_KEY=/etc/puppetlabs/puppet/ssl/private_keys/foreman.ovirt.iks.lab.pem
	#export FOREMAN_SSL_VERIFY=False
	export FOREMAN_SSL_VERIFY=/etc/puppetlabs/puppet/ssl/certs/ca.pem


##Step 2: Installazione Foreman wt. Puppet + Ansible plugin##

https://www.theforeman.org/manuals/1.16/quickstart_guide.html#QuickstartGuide

	yum -y install https://yum.puppetlabs.com/puppet5/puppet5-release-el-7.noarch.rpm
	
	yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
	yum -y install https://yum.theforeman.org/releases/1.16/el7/x86_64/foreman-release.rpm

	yum -y install foreman-installer

	foreman-installer

	yum install yum install tfm-rubygem-foreman-tasks


Success!
  * Foreman is running at https://foreman.ovirt.iks.lab
      Initial credentials are admin / qwerty
  * Foreman Proxy is running at https://foreman.ovirt.iks.lab:8443
  * Puppetmaster is running at port 8140
  The full log is at /var/log/foreman-installer/foreman.log

##Step 3: Install Foreman-ansible Plugin##
	
	yum install tfm-rubygem-foreman_ansible

	service httpd restart
	service foreman restart
	service foreman-proxy restart
	service foreman-tasks restart

Sources:
https://groups.google.com/forum/#!topic/foreman-users/4IvpG9a8Kk4


##Step 4: Install Ovirt, Vmware plugins:##

- Add foreman-tasks: necessario per eseguire gli Ansible-roles sugli host
	
	yum install tfm-rubygem-hammer_cli_foreman_tasks
	service foreman restart

- Add ovirt_provision_plugin: https://github.com/theforeman/ovirt_provision_plugin
	
	yum install tfm-rubygem-ovirt_provision_plugin
	service foreman restart
	service foreman-proxy restart

- Foreman - Ovirt <https://www.theforeman.org/manuals/1.16/index.html#5.2.7oVirt/RHEVNotes>

	command: yum -y install foreman-ovirt

- Install discovery plugin: necessario ad ottenere il collegamento con gli external providers, es. VMware

	yum install tfm-rubygem-foreman_discovery
	service foreman restart

[ovirt-users] Foreman: Add external provider: http://lists.ovirt.org/pipermail/users/2014-November/028979.html

- Install VMware plugin e tfm-rubygem-foreman_vmwareannotations: fatto questo appare VMware tra gli external-providers 

	yum install foreman-vmware
	yum install tfm-rubygem-foreman_vmwareannotations
	service foreman restart
	service foreman-proxy restart