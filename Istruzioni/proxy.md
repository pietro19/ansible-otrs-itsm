# Indirizzo #
http://bld_prx:Password1@x-proxy2.iks.local:3128
## Configurare git ##
	git config --global http.proxy http://bld_prx:Password1@x-proxy2.iks.local:3128

If you decide at any time to reset this proxy and work without proxy:
Command to use:
	
	git config --global --unset http.proxy
Finally, to check the currently set proxy:
	
	git config --global --get http.proxy

## Variabile globale ##
export http_proxy=http://bld_prx:Password1@x-proxy2.iks.local:3128
export https_proxy=https://bld_prx:Password1@x-proxy2.iks.local:3128