# OTRS Import-Export da console #

## Import ##

```
perl otrs.Console.pl Admin::ITSM::ImportExport::Import --help

The tool for importing config items

Usage:
 otrs.Console.pl Admin::ITSM::ImportExport::Import --template-number ... source

Options:
 --template-number ...          - Specify a template number to be impoerted.
 [--help]                       - Display help for this command.
 [--no-ansi]                    - Do not perform ANSI terminal output coloring.
 [--quiet]                      - Suppress informative output, only retain error messages.

Arguments:
 source                         - Specify the path to the file which containing the config item data for importing.

```
Dall'utente otrs (per cui su - otrs da root)
```
perl /opt/otrs/bin/otrs.Console.pl Admin::ITSM::ImportExport::Import --template-number 000003 /opt/otrs/CMDB_Import/import.csv
```
Dall'utente root:
```
su -c 'perl /opt/otrs/bin/otrs.Console.pl Admin::ITSM::ImportExport::Import --template-number 000006 /tmp/Ansible_Import_VM_Hosts.csv' -s /bin/bash otrs
```
## Export ##
```
perl otrs.Console.pl Admin::ITSM::ImportExport::Export --help

The tool for exporting config items

Usage:
 otrs.Console.pl Admin::ITSM::ImportExport::Export --template-number ... destination

Options:
 --template-number ...          - Specify a template number to be exported.
 [--help]                       - Display help for this command.
 [--no-ansi]                    - Do not perform ANSI terminal output coloring.
 [--quiet]                      - Suppress informative output, only retain error messages.

Arguments:
 destination                    - Specify the path to a file where config item data should be exported.
```

Dall'utente otrs (per cui su - otrs da root)
```
perl /opt/otrs/bin/otrs.Console.pl Admin::ITSM::ImportExport::Export --template-number 000003 /opt/otrs/CMDB_Import/export.csv
```
Dall'utente root:
```
su -c 'perl /opt/otrs/bin/otrs.Console.pl Admin::ITSM::ImportExport::Export --template-number 000003 /opt/otrs/CMDB_Import/export.csv' -s /bin/bash otrs
```