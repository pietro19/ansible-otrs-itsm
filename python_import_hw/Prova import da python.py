result = {'_ansible_parsed': True, '_ansible_no_log': False, 'changed': False, '_ansible_verbose_override': True, 'failed': False, 'invocation': {'module_args': {'filter': '*', 'gather_subset': ['all'], 'fact_path': '/etc/ansible/facts.d', 'gather_timeout': 10}}, 'ansible_facts': {'ansible_product_serial': 'VMware-42 0b a5 ee d1 25 97 59-01 f1 ca d2 35 9c 58 b1', 'ansible_form_factor': 'Other', 'ansible_distribution_file_parsed': True, 'ansible_fips': False, 'ansible_service_mgr': 'systemd', 'ansible_user_id': 'root', 'ansible_selinux_python_present': True, 'ansible_memtotal_mb': 2847, 'ansible_all_ipv4_addresses': ['192.168.101.213'], 'gather_subset': ['all'], 'ansible_ssh_host_key_ecdsa_public': 'AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHwmXjUZH11iwd7zDKmNOa+zFY5kPRgawn86zlVSdWp4WDnRirLOcSWQwzkgJoaH7Xtvo/MmphtDCARju5hoBKM=', 'ansible_domain': 'iks.local', 'ansible_distribution_version': '7.4.1708', 'ansible_local': {}, 'ansible_distribution_file_path': '/etc/redhat-release', 'ansible_device_links': {'masters': {'sda2': ['dm-0', 'dm-1']}, 'labels': {}, 'ids': {'sr0': ['ata-VMware_Virtual_SATA_CDRW_Drive_00000000000000000001'], 'sda2': ['lvm-pv-uuid-A28T1k-i8yg-xHrt-8e70-z87N-Jb8K-oz6i4g'], 'dm-0': ['dm-name-centos-root', 'dm-uuid-LVM-SPKF1yPllgg4IoueHox1ROBC3JcQpf5SMxmEbUMqIhg0gu1koRTNSOPec5N64Eo'], 'dm-1': ['dm-name-centos-swap', 'dm-uuid-LVM-SPKF1yPllgg4IoueHox1ROBC3JcQpf5SrxWMxTdRT973x1IoJnI8q65ltdIePd2d']}, 'uuids': {'dm-1': ['7dd20034-8a61-437f-bc6d-5e52c09f966e'], 'dm-0': ['a7d3a387-b56c-43cf-addb-7223dbca9e83'], 'sda1': ['40677ddd-fb5d-4c83-98e8-004a480fca2f']}}, 'ansible_date_time': {'weekday_number': '1', 'iso8601_basic_short': '20171211T121031', 'tz': 'CET', 'weeknumber': '50', 'hour': '12', 'time': '12:10:31', 'tz_offset': '+0100', 'month': '12', 'epoch': '1512990631', 'iso8601_micro': '2017-12-11T11:10:31.138842Z', 'weekday': 'Monday', 'iso8601_basic': '20171211T121031138738', 'year': '2017', 'date': '2017-12-11', 'iso8601': '2017-12-11T11:10:31Z', 'day': '11', 'minute': '10', 'second': '31'}, 'ansible_real_user_id': 0, 'ansible_processor_cores': 1, 'ansible_virtualization_role': 'guest', 'ansible_distribution_file_variety': 'RedHat', 'ansible_env': {'LANG': 'en_US.UTF-8', 'TERM': 'xterm', 'SHELL': '/bin/bash', 'XDG_RUNTIME_DIR': '/run/user/0', 'SHLVL': '3', 'SSH_TTY': '/dev/pts/0', 'HOSTNAME': 'x-otrs-cmdb.iks.local', 'PWD': '/etc/ansible/dmidecode', 'LESSOPEN': '||/usr/bin/lesspipe.sh %s', 'HISTCONTROL': 'ignoredups', 'SSH_CLIENT': '192.168.100.100 52349 22', 'LOGNAME': 'root', 'USER': 'root', 'HISTSIZE': '1000', 'MAIL': '/var/spool/mail/root', 'SSH_CONNECTION': '192.168.100.100 52349 192.168.101.213 22', 'HOME': '/root', 'LS_COLORS': 'rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=01;36:*.au=01;36:*.flac=01;36:*.mid=01;36:*.midi=01;36:*.mka=01;36:*.mp3=01;36:*.mpc=01;36:*.ogg=01;36:*.ra=01;36:*.wav=01;36:*.axa=01;36:*.oga=01;36:*.spx=01;36:*.xspf=01;36:', 'XDG_SESSION_ID': '4662', '_': '/usr/bin/python', 'PATH': '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin'}, 'ansible_effective_group_id': 0, 'ansible_bios_version': '6.00', 'ansible_processor': ['0', 'GenuineIntel', 'Intel(R) Xeon(R) CPU E5-2640 v2 @ 2.00GHz'], 'ansible_virtualization_type': 'VMware', 'ansible_lo': {'mt': 65536, 'promisc': False, 'timestamping': ['rx_software', 'software'], 'features': {'tx_checksum_ipv4': 'off [fixed]', 'tx_checksum_ipv6': 'off [fixed]', 'tx_ipip_segmentation': 'off [fixed]', 'tx_lockless': 'on [fixed]', 'tx_tcp_ecn_segmentation': 'on', 'tx_gre_segmentation': 'off [fixed]', 'tx_checksumming': 'on', 'vlan_challenged': 'on [fixed]', 'tx_vlan_stag_hw_insert': 'off [fixed]', 'hw_tc_offload': 'off [fixed]', 'generic_segmentation_offload': 'on', 'tx_udp_tnl_segmentation': 'off [fixed]', 'rx_vlan_offload': 'off [fixed]', 'tx_nocache_copy': 'off [fixed]', 'tx_mpls_segmentation': 'off [fixed]', 'tx_udp_tnl_csum_segmentation': 'off [fixed]', 'udp_fragmentation_offload': 'on', 'scatter_gather': 'on', 'tx_sit_segmentation': 'off [fixed]', 'tx_checksum_fcoe_crc': 'off [fixed]', 'tx_scatter_gather': 'on [fixed]', 'netns_local': 'on [fixed]', 'tx_gre_csum_segmentation': 'off [fixed]', 'generic_receive_offload': 'on', 'tx_scatter_gather_fraglist': 'on [fixed]', 'rx_all': 'off [fixed]', 'highdma': 'on [fixed]', 'rx_fcs': 'off [fixed]', 'tx_tcp6_segmentation': 'on', 'tx_gso_robust': 'off [fixed]', 'tx_tcp_mangleid_segmentation': 'on', 'loopback': 'on [fixed]', 'fcoe_mt': 'off [fixed]', 'tx_checksum_sctp': 'on [fixed]', 'l2_fwd_offload': 'off [fixed]', 'rx_vlan_stag_hw_parse': 'off [fixed]', 'tx_gso_partial': 'off [fixed]', 'rx_vlan_stag_filter': 'off [fixed]', 'large_receive_offload': 'off [fixed]', 'tx_checksum_ip_generic': 'on [fixed]', 'rx_checksumming': 'on [fixed]', 'tx_tcp_segmentation': 'on', 'busy_poll': 'off [fixed]', 'tcp_segmentation_offload': 'on', 'ntuple_filters': 'off [fixed]', 'rx_vlan_filter': 'off [fixed]', 'tx_sctp_segmentation': 'on', 'tx_fcoe_segmentation': 'off [fixed]', 'tx_vlan_offload': 'off [fixed]', 'receive_hashing': 'off [fixed]'}, 'ipv6': [{'scope': 'host', 'prefix': '128', 'address': '::1'}], 'device': 'lo', 'type': 'loopback', 'hw_timestamp_filters': [], 'active': True, 'ipv4': {'broadcast': 'host', 'netmask': '255.0.0.0', 'network': '127.0.0.0', 'address': '127.0.0.1'}}, 'ansible_userspace_bits': '64', 'ansible_architecture': 'x86_64', 'ansible_ssh_host_key_ed25519_public': 'AAAAC3NzaC1lZDI1NTE5AAAAIGGTsKMfOg9mp4g8GyUPy0pwFo55a8wOp73i3v+pUwXw', 'ansible_default_ipv4': {'broadcast': '192.168.101.255', 'macaddress': '00:50:56:8b:6f:fc', 'netmask': '255.255.255.0', 'alias': 'ens192', 'network': '192.168.101.0', 'mt': 1500, 'interface': 'ens192', 'address': '192.168.101.213', 'type': 'ether', 'gateway': '192.168.101.254'}, 'ansible_swapfree_mb': 1386, 'ansible_default_ipv6': {}, 'ansible_distribution_release': 'Core', 'ansible_system_vendor': 'VMware, Inc.', 'ansible_apparmor': {'status': 'disabled'}, 'ansible_cmdline': {'LANG': 'en_US.UTF-8', 'rhgb': True, 'BOOT_IMAGE': '/vmlinuz-3.10.0-693.5.2.el7.x86_64', 'crashkernel': 'auto', 'rd.lvm.lv': 'centos/swap', 'ro': True, 'root': '/dev/mapper/centos-root', 'quiet': True}, 'ansible_effective_user_id': 0, 'ansible_mounts': [{'block_used': 762041, 'size_total': 14879293440, 'block_available': 2870599, 'fstype': 'xfs', 'inode_total': 14540800, 'device': '/dev/mapper/centos-root', 'block_size': 4096, 'uuid': 'a7d3a387-b56c-43cf-addb-7223dbca9e83', 'block_total': 3632640, 'mount': '/', 'size_available': 11757973504, 'inode_available': 14466383, 'inode_used': 74417, 'options': 'rw,relatime,attr2,inode64,noquota'}, {'block_used': 51700, 'size_total': 520794112, 'block_available': 75447, 'fstype': 'xfs', 'inode_total': 512000, 'device': '/dev/sda1', 'block_size': 4096, 'uuid': '40677ddd-fb5d-4c83-98e8-004a480fca2f', 'block_total': 127147, 'mount': '/boot', 'size_available': 309030912, 'inode_available': 511655, 'inode_used': 345, 'options': 'rw,relatime,attr2,inode64,noquota'}], 'ansible_selinux': {'status': 'disabled'}, 'ansible_product_version': 'None', 'ansible_os_family': 'RedHat', 'ansible_userspace_architecture': 'x86_64', 'ansible_product_uuid': '420BA5EE-D125-9759-01F1-CAD2359C58B1', 'ansible_product_name': 'VMware Virtual Platform', 'ansible_pkg_mgr': 'yum', 'ansible_memfree_mb': 1176, 'ansible_devices': {'sr0': {'scheduler_mode': 'cfq', 'rotational': '1', 'vendor': 'NECVMWar', 'links': {'masters': [], 'labels': [], 'ids': ['ata-VMware_Virtual_SATA_CDRW_Drive_00000000000000000001'], 'uuids': []}, 'sectors': '2097151', 'sas_device_handle': None, 'sas_address': None, 'virtual': 1, 'host': 'SATA controller: VMware SATA AHCI controller', 'sectorsize': '512', 'removable': '1', 'support_discard': '0', 'model': 'VMware SATA CD00', 'size': '1024.00 MB', 'holders': [], 'partitions': {}}, 'sda': {'scheduler_mode': 'deadline', 'rotational': '1', 'vendor': 'VMware', 'links': {'masters': [], 'labels': [], 'ids': [], 'uuids': []}, 'sectors': '41943040', 'sas_device_handle': None, 'sas_address': None, 'virtual': 1, 'host': 'Serial Attached SCSI controller: VMware PVSCSI SCSI Controller (rev 02)', 'sectorsize': '512', 'removable': '0', 'support_discard': '0', 'model': 'Virtual disk', 'size': '20.00 GB', 'holders': [], 'partitions': {'sda2': {'sectorsize': 512, 'uuid': None, 'sectors': '32528384', 'links': {'masters': ['dm-0', 'dm-1'], 'labels': [], 'ids': ['lvm-pv-uuid-A28T1k-i8yg-xHrt-8e70-z87N-Jb8K-oz6i4g'], 'uuids': []}, 'start': '1026048', 'holders': ['centos-root', 'centos-swap'], 'size': '15.51 GB'}, 'sda1': {'sectorsize': 512, 'uuid': '40677ddd-fb5d-4c83-98e8-004a480fca2f', 'sectors': '1024000', 'links': {'masters': [], 'labels': [], 'ids': [], 'uuids': ['40677ddd-fb5d-4c83-98e8-004a480fca2f']}, 'start': '2048', 'holders': [], 'size': '500.00 MB'}}}, 'dm-0': {'scheduler_mode': '', 'rotational': '1', 'vendor': None, 'links': {'masters': [], 'labels': [], 'ids': ['dm-name-centos-root', 'dm-uuid-LVM-SPKF1yPllgg4IoueHox1ROBC3JcQpf5SMxmEbUMqIhg0gu1koRTNSOPec5N64Eo'], 'uuids': ['a7d3a387-b56c-43cf-addb-7223dbca9e83']}, 'sectors': '29081600', 'sas_device_handle': None, 'sas_address': None, 'virtual': 1, 'host': '', 'sectorsize': '512', 'removable': '0', 'support_discard': '0', 'model': None, 'size': '13.87 GB', 'holders': [], 'partitions': {}}, 'dm-1': {'scheduler_mode': '', 'rotational': '1', 'vendor': None, 'links': {'masters': [], 'labels': [], 'ids': ['dm-name-centos-swap', 'dm-uuid-LVM-SPKF1yPllgg4IoueHox1ROBC3JcQpf5SrxWMxTdRT973x1IoJnI8q65ltdIePd2d'], 'uuids': ['7dd20034-8a61-437f-bc6d-5e52c09f966e']}, 'sectors': '3358720', 'sas_device_handle': None, 'sas_address': None, 'virtual': 1, 'host': '', 'sectorsize': '512', 'removable': '0', 'support_discard': '0', 'model': None, 'size': '1.60 GB', 'holders': [], 'partitions': {}}}, 'ansible_user_uid': 0, 'ansible_lvm': {'pvs': {'/dev/sda2': {'free_g': '0.04', 'size_g': '15.51', 'vg': 'centos'}}, 'lvs': {'root': {'size_g': '13.87', 'vg': 'centos'}, 'swap': {'size_g': '1.60', 'vg': 'centos'}}, 'vgs': {'centos': {'free_g': '0.04', 'size_g': '15.51', 'num_lvs': '2', 'num_pvs': '1'}}}, 'ansible_distribution': 'CentOS', 'ansible_user_dir': '/root', 'ansible_dns': {'nameservers': ['192.168.100.20', '192.168.100.6'], 'search': ['iks.local']}, 'ansible_distribution_major_version': '7', 'module_setup': True, 'ansible_processor_count': 1, 'ansible_hostname': 'x-otrs-cmdb', 'ansible_processor_vcpus': 1, 'ansible_swaptotal_mb': 1639, 'ansible_lsb': {}, 'ansible_real_group_id': 0, 'ansible_bios_date': '04/05/2016', 'ansible_all_ipv6_addresses': ['fe80::2af5:ee09:558:189e'], 'ansible_interfaces': ['lo', 'ens192'], 'ansible_uptime_seconds': 1290965, 'ansible_machine_id': '3b042a4d65004f60a15de9ab32ca53d4', 'ansible_ssh_host_key_rsa_public': 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDN+fV9bkfIApETTr6Fq8jUDuepTeRWkGOx2OHJvwNHzfGCroXKL+ZB3aJYDYLaVwb6n9ZKgdki+t2/8bHOIbzKDMYWQLl30ZuaMBbwf3GgwmXViN6kkW2YvuMZiU5FVBNuF0sypYL3DUlJbq+/b6uPh4AUJNp0M9aTz16MspMbD4QQrYl9UgE3fdTUlaYpjGcUUB8pUXJKlbAkwfXzYPscI5wTW33mXb/BZPI/VV4Jj+55GPpLl8n4VAPAyQdCIoCLfQg7JXij+CxFH8RlbCzySwXD++JF3gyGWkoZVhtQ4XaB39MFU9RriGFVmq849ZdGuWj0bRpErXnABFuK3Qth', 'ansible_machine': 'x86_64', 'ansible_user_gecos': 'root', 'ansible_ens192': {'features': {'tx_checksum_ipv4': 'off [fixed]', 'tx_checksum_ipv6': 'off [fixed]', 'tx_ipip_segmentation': 'off [fixed]', 'tx_lockless': 'off [fixed]', 'tx_tcp_ecn_segmentation': 'off [fixed]', 'tx_gre_segmentation': 'off [fixed]', 'tx_checksumming': 'on', 'vlan_challenged': 'off [fixed]', 'tx_vlan_stag_hw_insert': 'off [fixed]', 'hw_tc_offload': 'off [fixed]', 'generic_segmentation_offload': 'on', 'tx_udp_tnl_segmentation': 'off [fixed]', 'rx_vlan_offload': 'on', 'tx_nocache_copy': 'off', 'tx_mpls_segmentation': 'off [fixed]', 'tx_udp_tnl_csum_segmentation': 'off [fixed]', 'udp_fragmentation_offload': 'off [fixed]', 'scatter_gather': 'on', 'tx_sit_segmentation': 'off [fixed]', 'tx_checksum_fcoe_crc': 'off [fixed]', 'tx_scatter_gather': 'on', 'netns_local': 'off [fixed]', 'tx_gre_csum_segmentation': 'off [fixed]', 'generic_receive_offload': 'on', 'tx_scatter_gather_fraglist': 'off [fixed]', 'rx_all': 'off [fixed]', 'highdma': 'on', 'rx_fcs': 'off [fixed]', 'tx_tcp6_segmentation': 'on', 'tx_gso_robust': 'off [fixed]', 'tx_tcp_mangleid_segmentation': 'off', 'loopback': 'off [fixed]', 'fcoe_mt': 'off [fixed]', 'tx_checksum_sctp': 'off [fixed]', 'l2_fwd_offload': 'off [fixed]', 'rx_vlan_stag_hw_parse': 'off [fixed]', 'tx_gso_partial': 'off [fixed]', 'rx_vlan_stag_filter': 'off [fixed]', 'large_receive_offload': 'on', 'tx_checksum_ip_generic': 'on', 'rx_checksumming': 'on', 'tx_tcp_segmentation': 'on', 'busy_poll': 'off [fixed]', 'tcp_segmentation_offload': 'on', 'ntuple_filters': 'off [fixed]', 'rx_vlan_filter': 'on [fixed]', 'tx_sctp_segmentation': 'off [fixed]', 'tx_fcoe_segmentation': 'off [fixed]', 'tx_vlan_offload': 'on', 'receive_hashing': 'off [fixed]'}, 'pciid': '0000:0b:00.0', 'module': 'vmxnet3', 'active': True, 'promisc': False, 'timestamping': ['rx_software', 'software'], 'device': 'ens192', 'speed': 10000, 'macaddress': '00:50:56:8b:6f:fc', 'mt': 1500, 'ipv4': {'broadcast': '192.168.101.255', 'netmask': '255.255.255.0', 'network': '192.168.101.0', 'address': '192.168.101.213'}, 'ipv6': [{'scope': 'link', 'prefix': '64', 'address': 'fe80::2af5:ee09:558:189e'}], 'type': 'ether', 'hw_timestamp_filters': []}, 'ansible_system_capabilities_enforced': 'True', 'ansible_python': {'executable': '/usr/bin/python', 'version': {'micro': 5, 'major': 2, 'releaselevel': 'final', 'serial': 0, 'minor': 7}, 'type': 'CPython', 'has_sslcontext': True, 'version_info': [2, 7, 5, 'final', 0]}, 'ansible_processor_threads_per_core': 1, 'ansible_fqdn': 'x-otrs-cmdb.iks.local', 'ansible_user_gid': 0, 'ansible_memory_mb': {'real': {'total': 2847, 'free': 1176, 'used': 1671}, 'swap': {'cached': 38, 'total': 1639, 'used': 253, 'free': 1386}, 'nocache': {'used': 612, 'free': 2235}}, 'ansible_python_version': '2.7.5', 'ansible_system': 'Linux', 'ansible_user_shell': '/bin/bash', 'ansible_system_capabilities': ['cap_chown', 'cap_dac_override', 'cap_dac_read_search', 'cap_fowner', 'cap_fsetid', 'cap_kill', 'cap_setgid', 'cap_setuid', 'cap_setpcap', 'cap_linux_immutable', 'cap_net_bind_service', 'cap_net_broadcast', 'cap_net_admin', 'cap_net_raw', 'cap_ipc_lock', 'cap_ipc_owner', 'cap_sys_module', 'cap_sys_rawio', 'cap_sys_chroot', 'cap_sys_ptrace', 'cap_sys_pacct', 'cap_sys_admin', 'cap_sys_boot', 'cap_sys_nice', 'cap_sys_resource', 'cap_sys_time', 'cap_sys_tty_config', 'cap_mknod', 'cap_lease', 'cap_audit_write', 'cap_audit_control', 'cap_setfcap', 'cap_mac_override', 'cap_mac_admin', 'cap_syslog', '35', '36+ep'], 'ansible_kernel': '3.10.0-693.5.2.el7.x86_64', 'ansible_nodename': 'x-otrs-cmdb.iks.local'}}
host = "capomastro.ovirt.iks.lab"

import socket
from socket import AF_INET, SOCK_STREAM, SOCK_DGRAM
import psutil

import datetime
import time
#fine nuovo

from ansible.plugins.callback import CallbackBase

import csv, operator
import json, os, rpm
import re

PATH = '../../../opt/otrs/CMDB_Import'
FILE_NAME = 'Ansible_Import_Hosts.csv'
FILE_NAME_VM = 'Ansible_Import_VM_Hosts.csv'
PACKAGES = './interesting_packages'


interesting_packages = []
if os.path.exists(PACKAGES):
    with open(PACKAGES, 'rb') as file:
        file.seek(0)
        interesting_packages=file.read().split()

def check_installed():
    ts = rpm.TransactionSet()
    mi = ts.dbMatch()
    installed = []
    for h in mi:
        installed.append(h['name'])
    results = []
    if interesting_packages: #lista contiene elementi
        interesting_installed = []
        for i in interesting_packages:
            r = re.compile(i)
            interesting_installed.extend(filter(r.match, installed))
        sortedresults = sorted(interesting_installed, key=operator.itemgetter(0))
        results = ", ".join(sortedresults)
    return results


print check_installed()

def extract_data(host, result_json):
    ''' Gather data to the sheet'''
    name=host
    serial_no = result_json['ansible_product_serial']
    fqdn = result_json['ansible_fqdn']
    opsys = result_json['ansible_distribution'] + " " + result_json['ansible_distribution_major_version']
    installed_sw = check_installed()
    cpu_name = max(result_json['ansible_processor'], key=len)
    cpu_number = result_json['ansible_processor_count']
    cpu_cores = result_json['ansible_processor_cores']
    ram = result_json['ansible_memory_mb']['real']['total']
    hard_disks = []
    n_hdd = 0
    for hd in result_json['ansible_devices']:
    	size = result_json['ansible_devices'][hd]['size']
        hdd_name = result_json['ansible_devices'][hd]['model']
        if hdd_name is None:
            hdd_name = result_json['ansible_devices'][hd]['links']['ids'][0]
        if n_hdd<10:
            hard_disks.extend([str(hdd_name), str(size)])
            n_hdd+=1
    for i in xrange(n_hdd, 10):
        hard_disks.extend(["", ""])
    ipv4 = result_json['ansible_all_ipv4_addresses'][0]
    ipv6 = result_json['ansible_all_ipv6_addresses'][0]
    exposed_srvcs = tcp_scs()
    row = [name, "Production", "Operational", serial_no, fqdn, opsys, installed_sw, cpu_name, cpu_number, cpu_cores, str(ram)+" MB"]
    row.extend(hard_disks)
    row.extend([ipv4, ipv6, exposed_srvcs])
    virt_role = result_json['ansible_virtualization_role']
    os.chdir(PATH)
    file = FILE_NAME if (result_json['ansible_virtualization_role'] == "host") else FILE_NAME_VM
    if (virt_role == "host"):
        file = FILE_NAME
    if not os.path.exists(file):
        create_report_file(file)
    write_data(file, row)
    rename_file(file)


def create_report_file(file):
    ''' Create the initial file if it doesn't exists'''
    os.chdir(PATH)
    open(file, 'a').close()

def write_data(file, row):
    ''' Write host data to csv'''
    os.chdir(PATH)
    with open(file) as f:
        with open("tmp-%s" % (file), "w") as csvfile:
            for line in f:
                csvfile.write(line)
            writer = csv.writer(
                csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
            writer.writerow(row)

def rename_file(file):
    os.chdir(PATH)
    os.remove(file)
    os.rename("tmp-%s" % (file), file)

def tcp_scs():
    results = ""
    AD = "-"
    AF_INET6 = getattr(socket, 'AF_INET6', object())
    proto_map = {
        (AF_INET, SOCK_STREAM): 'tcp',
        (AF_INET6, SOCK_STREAM): 'tcp6',
    #    (AF_INET, SOCK_DGRAM): 'udp',
    #    (AF_INET6, SOCK_DGRAM): 'udp6',
    }
    templ = "- Program name:%s; PID:%s;Proto: %s; \n    Local address: %s; Remote address:%s; Status: %s.\n"
    proc_names = {}
    for p in psutil.process_iter(attrs=['pid', 'name']):
        proc_names[p.info['pid']] = p.info['name']
    
    for c in psutil.net_connections(kind='inet'):
        pname = proc_names.get(c.pid, '?')[:15]
        for i in interesting_packages:
            r = re.compile(".*"+i+".*")
            if re.match(r, pname):
                laddr = "%s:%s" % (c.laddr)
                raddr = ""
                if c.raddr:
                    raddr = "%s:%s" % (c.raddr)
                results+=(templ % (
                    c.pid or AD,
                    pname,
                    proto_map[(c.family, c.type)],
                    laddr,
                    raddr or AD,
                    c.status
                ))
    if results:
        ts = time.time()
        st = "        timestamp: "+datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        results += st
    
    return results

print tcp_scs()
