#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type


import socket
from socket import AF_INET, SOCK_STREAM, SOCK_DGRAM

import csv, operator
import json, os

import requests
import json

url = "http://192.168.141.7/api/v1/hardware"

my_api_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjM0YjY4ZmI0MDQyYzczMGE1ZDA5Y2E4OTcyMWMxNzEzMzMyN2E3YjBjYmRhZjBhY2Q0NGEwMTJjYTc4NWRkMzE4ZTU1MTUxMGFkNzg3M2QyIn0.eyJhdWQiOiIxIiwianRpIjoiMzRiNjhmYjQwNDJjNzMwYTVkMDljYTg5NzIxYzE3MTMzMzI3YTdiMGNiZGFmMGFjZDQ0YTAxMmNhNzg1ZGQzMThlNTUxNTEwYWQ3ODczZDIiLCJpYXQiOjE1MTM3MDA2NTksIm5iZiI6MTUxMzcwMDY1OSwiZXhwIjoxNTQ1MjM2NjU5LCJzdWIiOiIyNDYiLCJzY29wZXMiOltdfQ.eXjH2wyGXIIZYlnlPot5JUFfuSlaIhIJh1WGxdj7aDJwW8ZIga95UGJBMKpDWiS1x5oItTLINykdKCRoNQNartcJFNcyDSNFzl0MuWuNH6v5BoAKsAhNmeD7T9lEKsBQzW1JToWr37ik6ePBgovdk24-oltxMYVHc43sMILm-jwrSRTyxMoBK4BxWBNe3UjIgbxlP7QoaWwq1enyjOemKjRW3dSC-Oq4tD6m-Q6xbsza6AWklvk_NC_mFhcUVVElAATLBjwzLHYZ9svE4C_8KsqfgbewhFoqYCn1HLRIfS3q0mz1ofjg0gBDqfCPFIj803pBLZgm-JcQyTgPHPcr269eJhKDrZHRutknxtb7uYmv8voU9u_Gyzabt2RiVMBuiClECJwXOKwDS3KJbuiDNYCD_fO1FrRlyGOn9_Y4YOsMlU8BCxslWf_MvHk9qxu_A5Q-14HalHQcYvFQFs4xDcuzS48Tdt_97lL9OBiessZeRpvrNXJxk0pqkDiGwVcdfUGApuoUGEa-NGyizvxZb2PFis-CO3ykNSCXW4c2-yfDr_uwnfqqUQsVyzoUbHGTwSckj9sM_ZG7geIdxBxDkK4Q6tgxGgQA83zsDzvzKJ2Ava7pSjUpXSmbsvrjnvjkv_FnMgYzR-MMboWLLYN1U_iNLBGPFX5utB5PREnpkyI"

headers = {"Authorization":"Bearer "+my_api_token, "Accept":"application/json"}

def host_export():
    def send_data(payload):
        response = requests.post(url, data=payload, headers=headers)
        response_native = json.loads(response.text)
        
        msg = response_native['messages']
        if type(response_native['messages']) is unicode:
            to_print="L'invio dei dati dell'host ha avuto come risposta: " + response_native['status'] + "; " + msg
        else:
            to_print="L'invio dei dati dell'host ha avuto come risposta: " + response_native['status'] + "; "
            for i in msg:
                to_add = i + ": " + msg[i][0]
                to_print += to_add

        print(to_print)

        with open('result', 'w') as outfile:
            outfile.write(response.text)


    def extract_data(host, result_json):
        ''' Gather data on the host''' 
        category='Virtual Machine'
        #check if is not a VM
        if (result_json['ansible_virtualization_role'] == "host"):
            category = 'Server'
        host_info=[['name', host],
            ['category', category],
            ['model_id', 86],
            ['serial', result_json['ansible_product_serial']],
            ['status_id', 5],
            ['asset_tag', result_json['ansible_fqdn']],
            ['order_number', '']]
        return host_info

    def run():
        try:
            if result['ansible_facts']:
                host_info=extract_data(host, result['ansible_facts'])
                #print(host_info)
                with open("./host_data", "w") as file:
                    file.write(json.dumps(host_info))
                send_data(host_info)
        except:
            pass

host_export().run()