# Compiti per le vacanze #

1. Installare Foreman + Ansible plugin, configurare SmartProxy
2. Preparare Ansible role che va a scrivere i dati dell'host in Snipe
3. Far mandare i dati dell'host ad OTRS::ITSM
4. Interfacciare Foreman con Ovirt e VMware
5. Togliere Puppet all'installazione di Foreman

## Dubbi ##

- Comportamento da avere con macchine windows
- Comportamento con macchine non raggiungibili
- Stati da far assumere a macchine non raggiungibili

## Risorse#

- <http://docs.ansible.com/ansible/latest/uri_module.html>
- <https://theforeman.org/manuals/1.16/index.html#3.2.3InstallationScenarios>

