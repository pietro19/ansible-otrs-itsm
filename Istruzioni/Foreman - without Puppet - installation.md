# Installazione di Foreman senza Puppet, eseguita tramite Ansible#

##Step 1: Passi necessari per installare Foreman e Foreman-Proxy##
Eseguita a partire dal repository <https://github.com/adfinis-sygroup/foreman-ansible>, che contiene un ruolo configurabile che installa Foreman partendo dai packages. Dopo aver configurato i repository necessari, vengono installati:

- Un server mysql
- Isc_dhcp_server
- Un server tftp
- Foreman
- Foreman_proxy
- Passenger_nginx
- Infine vengono eseguiti dei file yml per configuare Foreman.

Come prima cosa va installato git e configurato in modo da permettergli di superare il proxy, i comandi sarebbero i seguenti, ma non hanno funzionato come dovevano.

	git config --global http.proxy http://bld_prx:Password1@x-proxy2.iks.local:312
	git clone https://github.com/adfinis-sygroup/foreman-ansible.git
	git submodule update --init

Il repository è stato allora copiato sul computer locale, la cartella foreman-ansible-master è stata poi trasferita sull'host remoto via ssh.

Sono state configurate le variabili necessarie nel file vars/example.yml:

	isc_dhcp_server_subnet:
	  - netaddress: 192.168.141.0
	    netmask: 255.255.255.0
	    gateway: 192.168.141.1
	    domain: ovirt.iks.lab
	    domain_search: ovirt.iks.lab iks.local
	    dns: 192.168.141.2
	    range: 192.168.141.181 192.168.141.210

Il comando usato per lanciare il playbook è stato:
	
	ansible-playbook foreman.yml
L'installazione si è però interrotta al momento di aggiungere le rpm-key nei ruoli che installano Foreman e Foreman-proxy.
Sfruttando il contenuto dei messaggi d'errore, sono state installate le chiavi manualmente, quindi sono state commentate le parti che utilizzavano il modulo rpm-key, nei file:

- ansible-foreman-master/ansible/roles/foreman/tasks/repository.yml,
- ansible-foreman-master/ansible/roles/foreman-proxy/tasks/repository.yml

In questo modo l'installazione è andata a buon fine: sono stati riavviati i servizi che compongono Foreman con i comandi che seguono:

	service httpd restart
	service foreman restart
	service foreman-proxy restart

Controllare su capomastro.ovirt.iks.lab se è tutto funzionante.

##Step 2: Post Installazione##

[Opzionale] Si è poi generata una CA per Foreman, visto che normalmente sarebbe gestita con Puppet, che però non è stato installato. I passi seguiti si trovano su:
https://datacenteroverlords.com/2012/03/01/creating-your-own-ssl-certificate-authority/
Sono state create:
	- rootCA.pem
	- capomastro.ovirt.iks.lab.crt
	- capomastro.ovirt.iks.lab.key

Impostare i repository per i plugins di Foreman, controllando che il file */etc/yum.repos.d/foreman_plugins.repo*, abbia il contenuto descritto alla pagina <https://www.theforeman.org/plugins/>.

Settare il server DHCP in Foreman-proxy:
- /etc/foreman-proxy/settings.yml, sono da controllare i campi
	- :foreman_url:
	- :http_port: 
- /etc/foreman-proxy/settings.d/dhcp.yml, sono da aggiungere:
	
		:dhcp_config: /etc/dhcp/dhcpd.conf
		:dhcp_leases: /var/lib/dhcpd/dhcpd.leases
Riavvia foreman-proxy, aggiungi il proxy in *Foreman/Infrastructure/Smart Proxy*

##Step 3: Installazione plugins:##

	All'interno di */etc/ansible/ansible.cnf*:

	- enable callbacks
	- whitelist foreman callback

Aggiungere il file */etc/profile.d/ansible.sh* con all'interno:

	export FOREMAN_URL=http://capomastro.ovirt.iks.lab
	export FOREMAN_SSL_VERIFY="/root/Pietro/rootCA.pem"
	export FOREMAN_SSL_CERT="/root/Pietro/capomastro.ovirt.iks.lab.crt"
	export FOREMAN_SSL_KEY="/root/Pietro/capomastro.ovirt.iks.lab.key"
Riavvia Foreman e Foreman-Proxy, controlla che sia disponibile il menu di Ansible

- Aggiungi foreman-tasks: necessario per eseguire gli Ansible-roles sugli host
	
	yum install tfm-rubygem-hammer_cli_foreman_tasks
	service foreman restart

- Aggiungi ovirt_provision_plugin: https://github.com/theforeman/ovirt_provision_plugin
	
	yum install tfm-rubygem-ovirt_provision_plugin
	service foreman restart
	service foreman-proxy restart

- Foreman - Ovirt <https://www.theforeman.org/manuals/1.16/index.html#5.2.7oVirt/RHEVNotes>

	command: yum -y install foreman-ovirt

- Installa discovery plugin: necessario ad ottenere il collegamento con gli external providers, es. VMware

	yum install tfm-rubygem-foreman_discovery
	service foreman restart

[ovirt-users] Foreman: Add external provider: http://lists.ovirt.org/pipermail/users/2014-November/028979.html

- Installa VMware plugin e tfm-rubygem-foreman_vmwareannotations: fatto questo appare VMware tra gli external-providers 

	yum install foreman-vmware
	yum install tfm-rubygem-foreman_vmwareannotations
	service foreman restart
	service foreman-proxy restart