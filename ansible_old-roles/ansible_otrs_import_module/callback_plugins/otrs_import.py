#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: otrs_import

short_description: Create a CSV for OTRS::ITSM

version_added: "2.4"

description:
    - "This module create a CSV file for OTRS::ITSM; the host params written match the mappings keys of the import/export template"

author:
    - Pietro Gabelli
'''

RETURN = '''
message:
    description: The data for *host* has been written in *file*
'''

#nuovo
import socket
from socket import AF_INET, SOCK_STREAM, SOCK_DGRAM
import psutil

import datetime
import time
#fine nuovo

from ansible.plugins.callback import CallbackBase

import csv, operator
import json, os, rpm
import re


PATH = './'
FILE_NAME = 'Ansible_Import_Hosts.csv'
FILE_NAME_VM = 'Ansible_Import_VM_Hosts.csv'
PACKAGES = './interesting_packages'


interesting_packages = []
if os.path.exists(PACKAGES):
    with open(PACKAGES, 'rb') as file:
        file.seek(0)
        interesting_packages=file.read().split()

def tcp_scs():
    results = ""
    AD = "-"
    AF_INET6 = getattr(socket, 'AF_INET6', object())
    proto_map = {
        (AF_INET, SOCK_STREAM): 'tcp',
        (AF_INET6, SOCK_STREAM): 'tcp6',
    #    (AF_INET, SOCK_DGRAM): 'udp',
    #    (AF_INET6, SOCK_DGRAM): 'udp6',
    }
    templ = "- Program name:%s; PID:%s;Proto: %s; \n    Local address: %s; Remote address:%s; Status: %s.\n"
    proc_names = {}
    for p in psutil.process_iter(attrs=['pid', 'name']):
        proc_names[p.info['pid']] = p.info['name']
    
    for c in psutil.net_connections(kind='inet'):
        pname = proc_names.get(c.pid, '?')[:15]
        for i in interesting_packages:
            r = re.compile(".*"+i+".*")
            if re.match(r, pname):
                laddr = "%s:%s" % (c.laddr)
                raddr = ""
                if c.raddr:
                    raddr = "%s:%s" % (c.raddr)
                results+=(templ % (
                    c.pid or AD,
                    pname,
                    proto_map[(c.family, c.type)],
                    laddr,
                    raddr or AD,
                    c.status
                ))
    if results:
        ts = time.time()
        st = "        timestamp: "+datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        results += st
    
    return results


def check_installed():
    ts = rpm.TransactionSet()
    mi = ts.dbMatch()
    installed = []
    for h in mi:
        installed.append(h['name'])
    results = []
    if interesting_packages: #lista contiene elementi
        interesting_installed = []
        for i in interesting_packages:
            r = re.compile(i)
            interesting_installed.extend(filter(r.match, installed))
        sortedresults = sorted(interesting_installed, key=operator.itemgetter(0))
        results = ", ".join(sortedresults)
    return results

def create_report_file(file):
    ''' Create the initial file if it doesn't exists'''
    os.chdir(PATH)
    open(file, 'a').close()

def write_data(file, row):
    ''' Write host data to csv'''
    os.chdir(PATH)
    with open(file) as f:
        with open("tmp-%s" % (file), "w") as csvfile:
            for line in f:
                csvfile.write(line)
            writer = csv.writer(
                csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
            writer.writerow(row)

def rename_file(file):
    os.chdir(PATH)
    os.remove(file)
    os.rename("tmp-%s" % (file), file)


def extract_data(host, result_json):
    ''' Gather data to the sheet'''
    name=host
    serial_no = result_json['ansible_product_serial']
    fqdn = result_json['ansible_fqdn']
    opsys = result_json['ansible_distribution'] + " " + result_json['ansible_distribution_major_version']
    installed_sw = check_installed()
    cpu_name = max(result_json['ansible_processor'], key=len)
    cpu_number = result_json['ansible_processor_count']
    cpu_cores = result_json['ansible_processor_cores']
    ram = result_json['ansible_memory_mb']['real']['total']
    hard_disks = []
    n_hdd = 0
    for hd in result_json['ansible_devices']:
        size = result_json['ansible_devices'][hd]['size']
        hdd_name = result_json['ansible_devices'][hd]['model']
        if hdd_name is None:
            hdd_name = result_json['ansible_devices'][hd]['links']['ids'][0]
        if n_hdd<10:
            hard_disks.extend([str(hdd_name), str(size)])
            n_hdd+=1
    for i in xrange(n_hdd, 10):
        hard_disks.extend(["", ""])
    ipv4 = result_json['ansible_all_ipv4_addresses'][0]
    ipv6 = result_json['ansible_all_ipv6_addresses'][0]
    exposed_srvcs = tcp_scs()
    row = [name, "Production", "Operational", serial_no, fqdn, opsys, installed_sw, cpu_name, cpu_number, cpu_cores, str(ram)+" MB"]
    row.extend(hard_disks)
    row.extend([ipv4, ipv6, exposed_srvcs])
    virt_role = result_json['ansible_virtualization_role']
    os.chdir(PATH)
    file = FILE_NAME if (result_json['ansible_virtualization_role'] == "host") else FILE_NAME_VM
    if (virt_role == "host"):
        file = FILE_NAME
    if not os.path.exists(file):
        create_report_file(file)
    write_data(file, row)
    rename_file(file)


class CallbackModule(CallbackBase):
    CALLBACK_VERSION = 3.0
    CALLBACK_TYPE = 'aggregate'
    CALLBACK_NAME = 'hostlist'
    CALLBACK_NEEDS_WHITELIST = True
    def __init__(self):
        super(CallbackModule, self).__init__()
    def runner_on_ok(self, host, result):
        try:
            if result['ansible_facts']:
                extract_data(host, result['ansible_facts'])
                file = FILE_NAME if (result['ansible_facts']['ansible_virtualization_role'] == "host") else FILE_NAME_VM
                self._display.display("The data of %s has been written in %s"%(host, file))
        except:
            pass

