# Aggiornamento di Snipe-it da v3 a v4 #
Fonte: https://snipe-it.readme.io/docs/upgrading-to-v4
Passi:
1. Go to Admin > Backups and generate a new backup. Download that file
2.Backup your old version
	cd /var/www
	mkdir snipe-it-bkup
	cp -R /var/www/snipe-it/ /var/www/snipe-it-bkup
3. Put Snipe-IT into maintenance mode
	php artisan down
4. Remove old cache files

	```
	rm -rf bootstrap/cache/config.php
	rm -rf bootstrap/cache/services.php
	rm -rf bootstrap/cache/compiled.php
	```

	Run:
	
	```
	composer dump
	php artisan cache:clear
	php artisan view:clear
	php artisan config:clear
	```
5. Download/clone the new repository
	Io l'ho piazzato nella cartella superiore, il file era snipe-it-master.zip
	unzip snipe-it-master.zip
	chown -R nginx:nginx snipe-it-master
	\cp -a snipe-it-master/. snipe-it-3.6.2/
6. Update dependencies
	facoltativo: 

	```
		composer update
		composer upgrade
	```
	```
		composer install --no-dev --prefer-source
		composer dump-autoload
	```
7. Copy over any new .env settings
	Open up your .env file in your Snipe-IT install directory, and add any missing .env variables in https://github.com/snipe/snipe-it/blob/v4.1.3/.env.example
8. Check permissions of the storage directory
9. Migrate the database

	```
		php artisan migrate
	```
10.a Recrypt any encrypted fields
	If the application has inssues [message like: "Oops!" or "Whoops!"], setting the .env variable APP_DEBUG=true, the debug mode will be on.
	If the issue is caused by the encription and its cypher, the steps are as follow:
	a. Open your .env file and add a new field called LEGACY_APP_KEY= and add your v3.x Snipe-IT APP_KEY value there.
	b. Also in your .env, add LEGACY_CIPHER=rijndael-256
	c. run php artisan key:generate to generate a new, non-mcrypt APP_KEY
	d. run php artisan config:clear to clear your config cache
	e. run php artisan snipeit:legacy-recrypt to decrypt and re-encrypt any encrypted custom fields
	f. clear your browser cookies
10.b Error Class 'Aws\Laravel\AwsServiceProvider' not found or Class 'Log' not found
	This typically happens when you have a config file left over from an upgrade that was previously deleted.
		delete the files (not the directory) in bootstrap/cache
		run composer dump-autoload
		remove config/aws.php if it exists
11. Restart the application

```
	php artisan up

```