#Foreman - Remove Pending tasks#
##For Foreman without Puppet##
1. First make su to foreman user

	su -s /bin/bash - foreman 
2.Execute mysql

	mysql 
3. connect to the foreman database
	
	\c foreman; 
4. Delete all the foreman task and foreman task locks
	
	delete from foreman_tasks_tasks; 
	delete from foreman_tasks_locks; 
5. Quit from postgres and exit

	\q 
	exit
6. Restart all the services
	
	for s in {qpidd,pulp_celerybeat,pulp_resource_manager,pulp_workers,httpd}; do sudo service $s restart; done 


##For Foreman with Puppet##
1. First make su to postgres user
	
	su -s /bin/bash - postgres 
2. Execute psql
	
	psql 
3. connect to the foreman database
	
	\c foreman; 
4. Delete all the foreman task and foreman task locks
	
	delete from foreman_tasks_tasks; 
	delete from foreman_tasks_locks; 
5. Quit from postgres and exit
	
	\q 
	exit 
6. Restart all the services
	
	for s in {qpidd,pulp_celerybeat,pulp_resource_manager,pulp_workers,httpd}; do sudo service $s restart; done 