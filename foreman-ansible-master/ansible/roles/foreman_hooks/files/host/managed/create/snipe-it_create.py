#!/usr/bin/env python
import sys
import tempfile
import subprocess

sys.path.append('/usr/share/foreman/config/hooks')

import json
import requests
from requests.auth import HTTPBasicAuth
from subprocess import call

from functions import (HOOK_EVENT, HOOK_OBJECT, HOOK_TEMP_DIR, get_json_hook)

snipe_url = "http://192.168.141.7/api/v1/hardware"
snipe_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjM0YjY4ZmI0MDQyYzczMGE1ZDA5Y2E4OTcyMWMxNzEzMzMyN2E3YjBjYmRhZjBhY2Q0NGEwMTJjYTc4NWRkMzE4ZTU1MTUxMGFkNzg3M2QyIn0.eyJhdWQiOiIxIiwianRpIjoiMzRiNjhmYjQwNDJjNzMwYTVkMDljYTg5NzIxYzE3MTMzMzI3YTdiMGNiZGFmMGFjZDQ0YTAxMmNhNzg1ZGQzMThlNTUxNTEwYWQ3ODczZDIiLCJpYXQiOjE1MTM3MDA2NTksIm5iZiI6MTUxMzcwMDY1OSwiZXhwIjoxNTQ1MjM2NjU5LCJzdWIiOiIyNDYiLCJzY29wZXMiOltdfQ.eXjH2wyGXIIZYlnlPot5JUFfuSlaIhIJh1WGxdj7aDJwW8ZIga95UGJBMKpDWiS1x5oItTLINykdKCRoNQNartcJFNcyDSNFzl0MuWuNH6v5BoAKsAhNmeD7T9lEKsBQzW1JToWr37ik6ePBgovdk24-oltxMYVHc43sMILm-jwrSRTyxMoBK4BxWBNe3UjIgbxlP7QoaWwq1enyjOemKjRW3dSC-Oq4tD6m-Q6xbsza6AWklvk_NC_mFhcUVVElAATLBjwzLHYZ9svE4C_8KsqfgbewhFoqYCn1HLRIfS3q0mz1ofjg0gBDqfCPFIj803pBLZgm-JcQyTgPHPcr269eJhKDrZHRutknxtb7uYmv8voU9u_Gyzabt2RiVMBuiClECJwXOKwDS3KJbuiDNYCD_fO1FrRlyGOn9_Y4YOsMlU8BCxslWf_MvHk9qxu_A5Q-14HalHQcYvFQFs4xDcuzS48Tdt_97lL9OBiessZeRpvrNXJxk0pqkDiGwVcdfUGApuoUGEa-NGyizvxZb2PFis-CO3ykNSCXW4c2-yfDr_uwnfqqUQsVyzoUbHGTwSckj9sM_ZG7geIdxBxDkK4Q6tgxGgQA83zsDzvzKJ2Ava7pSjUpXSmbsvrjnvjkv_FnMgYzR-MMboWLLYN1U_iNLBGPFX5utB5PREnpkyI"


headers = {"Authorization":"Bearer "+snipe_token, "Accept":"application/json"}

HOOK_JSON = get_json_hook()

# read the information received
if HOOK_JSON.get('host'):
    hostname = str(HOOK_JSON.get('host').get('host').get('name', None))

    mac_address = str(HOOK_JSON.get('host').get('mac', None))

    operating_system = str(HOOK_JSON.get('host').get('host').get('operatingsystem_name', None))

    ip = str(HOOK_JSON.get('host').get('host').get('ip', None))
    payload_create = {
      "name": hostname,
      "category": 'Virtual Machine',
      "model_id": 86,
      "serial": "",
      "status_id": 5,
      "asset_tag": hostname,
      "order_number": ''
    }

    response_get = requests.post(snipe_url, json=payload_create, headers=headers)