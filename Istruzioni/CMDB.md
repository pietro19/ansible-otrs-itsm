# CMDB #
##Elementi##
L'obiettivo del Configuration Management e' di fornire un modello logico dell'infrastruttura attraverso l'identificazione, il controllo, la gestione  e la verifica di tutte le versioni di "Configuration Items" esistenti.A questo punto, e' opportuno definire i Configuration Item (per maggiori dettagli andate su wikipedia: http://en.wikipedia.org/wiki/Configuration_item ).

Il configuration item, o CI, e' un unita' di configurazione (elemento dell'insfrastuttura IT) che puo' essere gestita individualmente (tipo: computer, routers, servers, software, etc...).

Un elemento chiave del processo e' il Configuration Management Database (CMDB), che viene utilizzato per tracciare tutte le CI e le relazioni tra di loro (tipo: il server A ospita il Servizio B, etc...).

Alcuni **benefici** dell'implementare il processo di configuration management sono i seguenti:

- Disponibilita' di informazioni accurate sull'infrastruttura IT
- Maggiore controllo sulle CI (potenzialmente costose)
- Maggiore aderenza alle leggi (es. numero licenze software)
- Miglior supporto al processo di Incident Management, soprattutto nella valutazione dell'impatto degli incidenti.
- Miglior supporto al processo di Problem Management, in particolare nell'ausilio fornito nell'analisi della "root cause". 

Le *attività* relative al processo Configuration Management sono le seguenti:

- Pianificazione, include le strategie, policy, obiettivi, ruoli e responsabilita' nel processo di Configuration Management. Altro elemento da tenere presente nella pianficazione e' la struttura del CMDB.
- Identificazione, include la selezione, identificazione e "labeling" delle CI.
Controllo, include il processo di assicurare che solo le CI autorizzate siano presenti nel CMDB. Tutto le CI possono essere modificate solo attraverso - il processo di Change Management (di cui diremo in seguito).
- Status Accounting, la gestione del ciclo di vita delle CI (da quando sono in test a quando sono rilasciate e poi "disposed")
- Verifica, include gli audit effettuati con lo scopo di verificare l'accuratezza del CMDB.

##Problemi##
- Una delle poche raccomandazioni di ITIL in termini di priorita' di implementazione dei processi e' quello di implementare i processi di configuration, change e release management insieme, e possibilimente di avere una funzione centralizzata per gestirli

- Un altro problema e' la confusione tra il processo di configuration management ed il CMDB. Non basta implementare il CMDB per avere il processo in piedi. Bisogna prima avere tutte le procedure (Pianificazione, Identificazione, Controllo, Status Accounting e Verifica) per la gestione ed il mantenimento del CMDB e poi si puo' popolare il CMDB, senno' si rischia che esso risulti obsoleto prima di essere disponibile.

- Un altro problema comune e' quello di non avere un adeguato livello di dettaglio delle CI nel CMDB. Se' e' eccessivo (troppo dettaglio) si rischia di dover tracciare tutto (quante tastiere e mouse ci sono) con un dispendio eccessivo di risorse. Se e' troppo alto si rischia di non avere informazioni significative dal CMDB. Come in tutte le cose, il CMDB va' analizzato e pianificato in modo che sia uno strumento che sia di aiuto e non un peso.

- Un problema comune e' lo scarso interesse del management (nessuno e' interessato ad implementarlo). Questo purtroppo e' conseguenza del fatto che i benefici sono in la' nel tempo e non immediatamente tangibili. E' fondamentale che i manager vengano informati dei benefici nell'implementare il Configuration Management.

- Alzi la mano chi non ne ha avuto esperienza: il processo viene percepito come troppo burocratico e viene regolarmente bypassato. A questo scopo e' fondamentale che tutti capiscono l'importanza ed i benefici del Configuration Management.

