#Python 2.7.6
#RestfulClient.py

import requests
from requests.auth import HTTPDigestAuth
import json

# Replace with the correct URL
url = "http://localhost/otrs/nph-genericinterface.pl/Webservice/TestTest/NewLink"
user = "root@localhost"
password = "asdfgh"
ticket_id1 = 6
ticket_id2 = 5
payload = {"SourceObject": 'Ticket',
	"SourceKey": ticket_id1,
	"TargetObject": 'Ticket',
	"TargetKey": ticket_id2,
	"Type": 'Normal',
	"State": 'Valid',
	"UserID": '2'}

url_ticket="http://localhost/otrs/nph-genericinterface.pl/Webservice/TestTest/NewTicket"
payload={
	"Type": "Problem"
	"Title": "Incidente automatizzato",
	"CustomerUser": "03",
	"UserLogin": "01",
	"Priority": "3 Normal",
	"Queue": "Otrs"
}
myResponse = requests.post(url_ticket, auth=(user, password), json=payload, verify=True)
myResponse

payload= {"UserLogin": "root@localhost",
"Password": "asdfgh",
"Ticket": {
    "Type": "Unclassified",
    "Title": "some title",
    "CustomerUser": "utente@prova.com",
    "Queue": "Raw",
    "State": "New",
    "PriorityID": "3",
    "MimeType": "text/plain"
	},
"Article": {
	"Subject": "some subject",
    "Body": "some body",
    "Charset": "utf8",
    "MimeType": "text/plain"
	}
}

myResponse = requests.post(url_ticket, json=payload)
myResponse
response_native = json.loads(myResponse.text)
response_native['TicketID']

url_link = "http://localhost/otrs/nph-genericinterface.pl/Webservice/TestTest/NewLink"

ticket_id2 = 5
payload2 = {
	"UserLogin": 'root@localhost',
	"Password": "asdfgh",
	"Data": {
		"SourceObject": 'Ticket',
		"SourceKey": 14,
		"TargetObject": 'Ticket',
		"TargetKey": 15,
		"Type": 'Normal',
		"State": 'Valid',
		"UserID": 1
		}
	}

myResponse2 = requests.post(url_link, json=payload2)
myResponse2
myResponse2 = requests.post(url_link, auth=(user, password), data=json.dumps(payload2), verify=True)
response_native2 = json.loads(myResponse2.text)
response_native2

payload3 = {
	"DebuggerObject": "",
	"WebserviceID": "",
}

myResponse3 = requests.post(url_link, auth=(user, password), json=payload3, verify=True)
myResponse3

#print (myResponse.status_code)

# For successful API call, response code will be 200 (OK)
if(myResponse.ok):

    # Loading the response data into a dict variable
    # json.loads takes in only binary or string variables so using content to fetch binary content
    # Loads (Load String) takes a Json file and converts into python data structure (dict or list, depending on JSON)
    jData = json.loads(myResponse.content)

    print("The response contains {0} properties".format(len(jData)))
    print("\n")
    for key in jData:
        print key + " : " + jData[key]
else:
  # If response code is not ok (200), print the resulting http error code with description
    myResponse.raise_for_status()





    #### Initialize new client session ####
$client = new SoapClient(
 null, 
 array(
 'location' => $url,
 'uri' => "Core",
 'trace' => 1,
 'login' => $username,
 'password' => $password,
 'style' => SOAP_RPC,
 'use' => SOAP_ENCODED
 )
);
#### Create and send the SOAP Function Call ####
$success = $client->__soapCall("Dispatch", 
array($username, $password,
"LinkObject", "LinkAdd",
"SourceObject", 'Ticket',
"SourceKey", $ticket_id1,
"TargetObject", 'Ticket',
"TargetKey", $ticket_id2,
"Type", 'ParentChild',
"State", 'Valid',
"UserID", '1'
));