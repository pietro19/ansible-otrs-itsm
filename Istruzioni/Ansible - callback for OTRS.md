# Ansible e Otrs::ITSM - Import macchine raggiungibili #

### Componenti aggiunti ###
Sono stati installati:

- Python
- Pip
- Dipendenze:

	- json
	- csv
	- os
	- psutil
	- difflib

Le dipendenze sono state controllate con il comando:

Psutil è stato installato importando il gituhub folder nell'host, installando il necessario a compilare i sorgenti ed installarli: 
	
	```
	sudo yum install gcc python-devel python-pip
	cd psutil
	python setup.py install
	```
Difflib è stata installata con il comando:

	```
	yum search difflib
	yum install php-phpspec-php-diff
	```

	```
	pip list
	```

## Ansible Callbacks ##
-  Modulo callback: si attiva su eventi che avvengono durante l'esecuzione di un playbook
-  I moduli supportati sono:

	- runner_on_failed
	- runner_on_ok
	- runner_on_skipped
	- runner_on_unreachable
	- runner_on_no_hosts
	- runner_on_async_poll
	- runner_on_async_ok
	- runner_on_async_failed
	- playbook_on_start
	- playbook_on_notify
	- playbook_on_no_hosts_matched
	- playbook_on_no_hosts_remaining
	- playbook_on_task_start
	- playbook_on_vars_prompt
	- playbook_on_setup
	- playbook_on_import_for_host
	- playbook_on_not_import_for_host
	- playbook_on_play_start
	- playbook_on_stats
	Sono dei callback points che possono essere utilizzati come triggers per impostare determinate azioni che partono da quei punti: quandoo l'esecuzione raggiunge ognuno degli stati, ogni plugin che ha del codice da eseguire eseguirà.

-  Il plugin deve avere una classe chiamata CallBack, grazie alla quale si identifica come un callback plugin
-  Il file deve essere posizionato nella directory callback_plugins che si trova nella radice dell'Ansible play

## Modulo otrs_import ##
- Si utilizza il modulo runner_on_ok, che viene attivato quando l'esecuzione va a buon fine
- Il plugin creato non fa altro che scrivere all'interno di un file CSV, per ogni host:

	- nome dell'host, 
	- Deployments State = "Production"
	- Incident State = "Operational",
	- Numero seriale
	- FQDN
	- Sistema Operativo

Utilizzo del plugin:

```
ansible-playbook otrs_import.yaml --forks 1
```
Per evitare che l'esecuzione parallela si scontri con la presenza di un unico file su cui scrivere, si esegue su di un host alla volta, impostando fork ad 1.

Nel caso si voglia eseguire su di un gruppo di host, es. solo su localhost:

```
ansible-playbook -l localhost dmidecode.yaml --forks 1
```