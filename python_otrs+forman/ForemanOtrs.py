# -*- coding: utf-8 -*-
from __future__ import print_function
try:
    # Python 2 version
    import ConfigParser
except ImportError:
    # Python 3 version
    import configparser as ConfigParser
import json, argparse, copy, os, re, sys, requests, datetime, time, csv, operator, difflib

from time import time
from collections import defaultdict
from distutils.version import LooseVersion, StrictVersion
from requests.auth import HTTPBasicAuth
from subprocess import call

if LooseVersion(requests.__version__) < LooseVersion('1.1.0'):
    print('This script requires python-requests 1.1 as a minimum version')
    sys.exit(1)

def json_format_dict(data, pretty=False):
    """Converts a dict to a JSON object and dumps it as a formatted string"""
    if pretty:
        return json.dumps(data, sort_keys=True, indent=2)
    else:
        return json.dumps(data)

class ForemanOTRS(object):
#Creazione array di facts  degli host di Foremna
    def __init__(self):
        self.facts = dict()   # Facts of each host
        self.session = None   # Requests session
        self.config_paths = [
            "./ForemanOtrs.ini",
            os.path.dirname(os.path.realpath(__file__)) + '/ForemanOtrs.ini',
        ]

    def read_settings(self):
        """Reads the settings from the ForemanOtrs.ini file"""

        config = ConfigParser.SafeConfigParser()
        config.read(self.config_paths)

        # Foreman API related
        try:
            self.foreman_url = config.get('foreman', 'url')
            self.foreman_user = config.get('foreman', 'user')
            self.foreman_pw = config.get('foreman', 'password')
            self.foreman_ssl_verify = config.getboolean('foreman', 'ssl_verify')
        except (ConfigParser.NoOptionError, ConfigParser.NoSectionError) as e:
            print("Error parsing configuration: %s" % e, file=sys.stderr)
            return False

        # OTRS API related
        try:
            self.otrs_base_url = config.get('otrs', 'url')
            self.otrs_new_ticket = config.get('otrs', 'new_ticket_method')
            self.otrs_new_link_method = config.get('otrs', 'new_link_method')
            self.otrs_search_ci_method = config.get('otrs', 'search_ci_method')
            self.otrs_get_ci_method = config.get('otrs', 'get_ci_method')
            self.otrs_update_ci_method = config.get('otrs', 'update_ci_method')
            self.otrs_create_ci_method = config.get('otrs', 'create_ci_method')

            self.otrs_user = config.get('otrs', 'user')
            self.otrs_pw = config.get('otrs', 'password')
            self.otrs_user_id = config.get('otrs', 'user_id')

            self.otrs_customeruser = config.get('otrs', 'customeruser')

            self.otrs_url_search = "%s%s" % (self.otrs_base_url, self.otrs_search_ci_method)
            self.otrs_url_get = "%s%s" % (self.otrs_base_url, self.otrs_get_ci_method)
            self.otrs_url_update_ci = "%s%s" % (self.otrs_base_url, self.otrs_update_ci_method)
            self.otrs_url_create_ci = "%s%s" % (self.otrs_base_url, self.otrs_create_ci_method)
            self.otrs_url_create_link = "%s%s" % (self.otrs_base_url, self.otrs_new_link_method)
            self.otrs_url_create_ticket = "%s%s" % (self.otrs_base_url, self.otrs_new_ticket)

        except (ConfigParser.NoOptionError, ConfigParser.NoSectionError) as e:
            print("Error parsing configuration: %s" % e, file=sys.stderr)
            return False

        return True

    def parse_cli_args(self):
        """Command line argument processing"""

        parser = argparse.ArgumentParser(description='Sync the hosts in OTRS based on Foreman host data')
        # parser.add_argument('--list', action='store_true', default=True, help='List instances (default: True)') #remove
        # parser.add_argument('--host', action='store', help='Get all the variables about a specific instance') #remove
        # self.args = parser.parse_args() #remove

    def _get_session(self):
        if not self.session:
            self.session = requests.session()
            self.session.auth = HTTPBasicAuth(self.foreman_user, self.foreman_pw)
            self.session.verify = self.foreman_ssl_verify
        return self.session

    def _get_json(self, url, ignore_errors=None, params=None):
        if params is None:
            params = {}
        params['per_page'] = 250

        page = 1
        results = []
        s = self._get_session()
        while True:
            params['page'] = page
            ret = s.get(url, params=params)
            if ignore_errors and ret.status_code in ignore_errors:
                break
            ret.raise_for_status()
            json = ret.json()
            # /hosts/:id has not results key
            if 'results' not in json:
                return json
            # Facts are returned as dict in results not list
            if isinstance(json['results'], dict):
                return json['results']
            # List of all hosts is returned paginaged
            results = results + json['results']
            if len(results) >= json['subtotal']:
                break
            page += 1
            if len(json['results']) == 0:
                print("Did not make any progress during loop. "
                      "expected %d got %d" % (json['total'], len(results)),
                      file=sys.stderr)
                break
        return results

    def _get_hosts(self):
        url = "%s/api/v2/hosts" % self.foreman_url

        params = {}

        return self._get_json(url, params=params)

    def _get_host_data_by_id(self, hid):
        url = "%s/api/v2/hosts/%s" % (self.foreman_url, hid)
        return self._get_json(url)

    def _get_facts_by_id(self, hid):
        url = "%s/api/v2/hosts/%s/facts" % (self.foreman_url, hid)
        return self._get_json(url)

    def _get_facts(self, host):
        """Fetch all host facts of the host"""

        ret = self._get_facts_by_id(host['id'])
        if len(ret.values()) == 0:
            facts = {}
        elif len(ret.values()) == 1:
            facts = list(ret.values())[0]
        else:
            raise ValueError("More than one set of facts returned for '%s'" % host)
        return facts
    

    def get_otrs_vms(self):
        # trova tutti i Configuration Item di tipo Virtual Machine; per ognuno trova i dati memoriazzati e crea un dizionario chiave-valore
        # la chiave è il nome/fqdn della VM, il valore è costituito dal JSON dei dati in OTRS
        payload_search = {
            "UserLogin": self.otrs_user,
            "Password": self.otrs_pw,
            "ConfigItem":{
                "Class": "Virtual Machine",
                "DeplStates": ['Expired', 'Inactive', 'Maintenance', 'Pilot', 'Planned', 'Production', 'Repair', 'Review', 'Test/QA']
            }
        }
        response_search = requests.get(self.otrs_url_search, json=payload_search)
        ids = json.loads(response_search.text)['ConfigItemIDs']
        otrs_vm_result = dict()
        for host_id in ids:
            payload_get = {
                "UserLogin": self.otrs_user,
                "Password":self.otrs_pw,
                "ConfigItemID": host_id
            }
            response_get = requests.get(self.otrs_url_get, json=payload_get)
            host_name = json.loads(response_get.text)['ConfigItem'][0]['Name']
            host_data = json.loads(response_get.text)['ConfigItem'][0]
            otrs_vm_result.update({host_name: host_data})

        return otrs_vm_result

    def search_host_otrs(self, host):
        payload_search = {
             "UserLogin": self.otrs_user,
            "Password": self.otrs_pw,
            "ConfigItem":{
                "Name": host,
                "Class": "Virtual Machine"
            }
        }
        response_search = requests.get(self.otrs_url_search, json=payload_search)
        native_response_search = json.loads(response_search.text)
        return native_response_search

    def get_foreman_inventory(self):

        self.groups = dict()
        foreman_hosts = dict()
        
        for host in self._get_hosts():
            dns_name = host['name']
            
            self.facts[dns_name] = self._get_facts(host)
            
            host_data =self._get_host_data_by_id(host['id'])
            myvar=self.facts[dns_name]
            myvar.update(host_data)
            # print(dns_name)

            CIXMLData = self.extract_data(dns_name, myvar)
            foreman_hosts.update({dns_name: CIXMLData})

        return foreman_hosts

    def clean_string(self, to_clean):
        for ch in ['\"','[', ']']:
            if ch in to_clean:
                to_clean = to_clean.replace(ch, '')
        return to_clean

# Il metodo extract_data mira a produrre i valori dell'host fornito, nel formato che segue:
#     value => {
#         "Class": "Virtual Machine",
#         "Name": host,
#         "DeplState": "Production",
#         "InciState": "Operational",
#         CIXMLData    => {
#             "CPU": 
#                     {
#                         "CPU": " Westmere E56xx/L56xx/X56xx (Nehalem-C)",
#                         "Cores": "2",
#                         "Number": "1"
#                     },
#                 "Datastore": "",
#                 "ExposedServices": "--None--",
#                 "FQDN": "foreman.ovirt.iks.lab",
#                 "GuestOS": "CentOS 7.4",
#                 "HardwareMachineVersion": "",
#                 "IPoverDHCP": "",
#                 "IPv4Address": "192.168.141.80",
#                 "IPv6Address": "",
#                 "InstalledSoftware": "--None--",
#                 "Memory": [
#                     {
#                         "Capacity": "1024.00 MB",
#                         "Memory": "ata-QEMU_DVD-ROM_QM00003"
#                     },
#                     {
#                         "Capacity": "10.00 GB",
#                         "Memory": "scsi-0QEMU_QEMU_HARDDISK_1659e3e3-76d5-4c29-b"
#                     },
#                     {
#                         "Capacity": "8.47 GB",
#                         "Memory": "dm-name-centos-root"
#                     },
#                     {
#                         "Capacity": "1.00 GB",
#                         "Memory": "dm-name-centos-swap"
#                     },
#                 ],
#                 "Owner": "",
#                 "Ram": "3786 MB",
#                 "SerialNumber": "20DC46B7-567A-11DF-A4D6-68EFBDF66A4C",
#                 "WarrantyExpirationDate": "2018-01-16"
#         }
#     };

    def extract_data(self, host, result_json):
        name=host
        serial_no = ""
        fqdn = host
        opsys = result_json['operatingsystem_name']
        installed_sw = ""
        cpu_name = ""
        cpu_number = ""
        cpu_cores = ""
        ram = ""
        hard_disks = []
        n_hdd = 0

        ipv4 = result_json['ip']
        ipv6 = result_json['ip6']
        if 'ansible_product_serial' in result_json:
            serial_no = result_json['ansible_product_serial']
        installed_sw = ""
        if 'ansible_processor' in result_json:
            cpu_data = self.clean_string(result_json['ansible_processor'])
            cpu_name= max(cpu_data.split(','), key=len)
        if 'ansible_processor_count' in result_json:
            cpu_number = result_json['ansible_processor_count']
        if 'ansible_processor_cores' in result_json:
            cpu_cores = result_json['ansible_processor_cores']
        if 'ansible_memory_mb::real::total' in result_json:
            ram_prov = result_json['ansible_memory_mb::real::total']
            ram = str(ram_prov)+" MB"
        if 'ansible_devices' in result_json:
            b=[]
            for key in result_json.keys():
                r = ((re.search(r"ansible_devices\:\:([\w\-]*)$", key)))
                if r:
                    b.append(r.group(1))
            for hd in b:
                size = ''
                hdd_name = ''
                if 'ansible_devices::' + hd + '::size' in result_json:
                    size = result_json['ansible_devices::' + hd + '::size']
                if 'ansible_devices::' + hd + '::links::ids' in result_json:
                        hdd_name = self.clean_string(result_json['ansible_devices::' + hd + '::links::ids'].split(',')[0])
                elif 'ansible_devices::' + hd + '::model' in result_json:
                    hdd_name = result_json['ansible_devices::' + hd + '::model']

                if n_hdd<10:
                    to_add = {"Memory": str(hdd_name), "Capacity": str(size)}
                    hard_disks.append(to_add)
                    n_hdd+=1
        for i in xrange(n_hdd, 10):
            hard_disks.append({"Memory": "", "Capacity": ""})
        
        exposed_srvcs = ""
        res = {
            "CPU": {
                    "CPU": cpu_name,
                    "Cores": cpu_cores,
                    "Number": cpu_number
                },
            "FQDN": fqdn,
            "GuestOS": opsys,
            "IPoverDHCP": 'Yes',
            "IPv4Address": ipv4,
            "IPv6Address": ipv6,
            "Memory": hard_disks,
            "Ram": ram,
            "SerialNumber": serial_no,
        }

        return res

    def new_otrs_ticket(self, host_id):
        # apri il ticket
        payload_ticket = {
            "UserLogin": self.otrs_user,
            "Password": self.otrs_pw,
            "Ticket": {
                "Type": "Unclassified",
                "Title": "VM indisponibile",
                "CustomerUser": self.otrs_customeruser,
                "Queue": "Postmaster",
                "State": "New",
                "PriorityID": "3",
                "MimeType": "text/plain"
                },
            "Article": {
                "Subject": "Ticket Automatico",
                "Body": "Non è stato possibile trovare la macchina nell'elenco degli host. Potrebbe essere stata eliminata",
                "Charset": "utf8",
                "MimeType": "text/plain"
            }
        }

        response_ticket = (requests.post(self.otrs_url_create_ticket, json=payload_ticket).text)
        native_response_ticket = json.loads(response_ticket)
        if 'TicketID' in native_response_ticket:
            # collega il Ticket appena creato al CI
            ticket_id = str(native_response_ticket['TicketID'])
            payload_link = {
                "UserLogin": self.otrs_user,
                "Password": self.otrs_pw,
                "SourceObject": 'Ticket',
                "SourceKey": ticket_id,
                "TargetObject": 'ITSMConfigItem',
                "TargetKey": host_id,
                "Type": 'DependsOn',
                "State": 'Valid',
                "UserID": self.otrs_user_id
                }
            
            # print(payload_link)
            myResponse = requests.post(self.otrs_url_create_link, json=payload_link)

            if myResponse.status_code == 200:
                to_print = "Aperto ticket %s su VM %s" % (ticket_id, host_id)
                print(to_print)

    def otrs_new_ci(self, host, host_data):
        # Crea la VM in OTRS, utilizzando i dati passati
        payload_new_ci = {
            "UserLogin": self.otrs_user,
            "Password": self.otrs_pw}
        payload_new_ci.update
        ConfigItem = {
                "Class": "Virtual Machine",
                "Name": host,
                "DeplState": "Production",
                "InciState": "Operational",
                }
        ConfigItem['CIXMLData'] = host_data
        payload_new_ci['ConfigItem'] = ConfigItem

        myResponse = requests.post(self.otrs_url_create_ci, json=payload_new_ci)
        if 'Error' in myResponse.text:
            to_print = "Non è stato possibile creare l'host"
            print(to_print)
        else:
            # print(json.loads(myResponse.text))
            number = json.loads(myResponse.text)['Number']
            if myResponse.status_code == 200:
                to_print = "Creato host %s numero %s" % (host, number)
                print(to_print)  

    def sync_otrs_foreman(self, foreman_hosts, otrs_hosts):
        old_fqdns = otrs_hosts.keys()
        new_fqdns = foreman_hosts.keys()

        removed = [item for item in old_fqdns if item not in new_fqdns]
        for host in removed:
            # open new ticket
            host_id = otrs_hosts[host]["ConfigItemID"]
            self.new_otrs_ticket(host_id)

        added = [item for item in new_fqdns if item not in old_fqdns]
        for host in added:
            # create host
            host_data = foreman_hosts[host]
            self.otrs_new_ci(host, host_data)

#parte run
    def run(self):
        # Read settings and parse CLI arguments
        if not self.read_settings():
            return False
        self.parse_cli_args()
        foreman_hosts = self.get_foreman_inventory()
        otrs_hosts = self.get_otrs_vms()
        self.sync_otrs_foreman(foreman_hosts, otrs_hosts)
        return True

ForemanOTRS().run()
