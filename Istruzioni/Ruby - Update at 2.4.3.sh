#!/bin/bash
yum remove ruby ruby-devel

cd /tmp

yum groupinstall "Development Tools"

yum install openssl-devel
yum install wget

# wget http://cache.ruby-lang.org/pub/ruby/2.1/ruby-2.1.2.tar.gz
# tar xvfvz ruby-2.1.2.tar.gz
# cd ruby-2.1.2

cat <<-END > /etc/profile.d/proxy.sh
export http_proxy=http://bld_prx:Password1@x-proxy2.iks.local:3128
END

cd /tmp
wget http://cache.ruby-lang.org/pub/ruby/2.4/ruby-2.4.3.tar.gz
tar xvfvz ruby-2.4.3.tar.gz
cd ruby-2.4.3
./configure
make
make install


gem update --system
gem install bundler

cat <<-END >> /etc/ansible/hosts
localhost       ansible_connection=local
END

cat <<-END >> /etc/profile.d/ansible.sh
export FOREMAN_INI_PATH=/etc/ansible/foreman.ini
export FOREMAN_URL=http://192.168.141.199
export foreman_url=http://192.168.141.199
export ANSIBLE_LOCAL_TEMP="~/.ansible/tmp"
END


# Foreman folder: 
# /opt/theforeman/tfm/root/usr/share/gems/gems

# cp -R /usr/local/lib/ruby/gems/2.4.0/gems/foreman_probing-0.0.1/ /opt/theforeman/tfm/root/usr/share/gems/gems/

# scl enable tfm bash

# yum install mysql-devel
# scl enable tfm "gem uninstall concurrent-ruby --version 1.0.5